package com.example.mycontacts

class ContactField {
    var contactId: Int = 0
    var fieldName: String = ""
    var valueName: String = ""


    constructor(contactId: Int, fieldName: String, valueName: String) {
        this.contactId = contactId
        this.fieldName = fieldName
        this.valueName = valueName
    }

}
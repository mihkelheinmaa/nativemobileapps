package com.example.mycontacts

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.marginStart
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.edit_contact.*


class EditContactActivity() : AppCompatActivity() {

    private lateinit var contactsRepo: ContactRepository
    private lateinit var adapter: RecyclerView.Adapter<*>
    lateinit var contactToEdit: Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_contact)

        contactsRepo = ContactRepository(this).open()
        adapter = DataRecycleViewAdapterAllContacts(this, contactsRepo, MainActivity())

        var keyID = intent.getIntExtra("selectedContactID", 0)
        contactToEdit = contactsRepo.getPersonByID(keyID)

        var contacts = contactsRepo.getAllContactsForPerson(contactToEdit)
        val contactFields = ArrayList<ContactField>()

        contacts.forEach {
            if (it.phone != "") {
                contactFields.add(ContactField(it.id, "Phone number", it.phone))
            }
            if (it.email != "") {
                contactFields.add(ContactField(it.id, "E-mail", it.email))
            }
            if (it.skype != "") {
                contactFields.add(ContactField(it.id, "Skype", it.skype))
            }
            if (it.address != "") {
                contactFields.add(ContactField(it.id, "Address", it.address))
            }
            if (it.comment != "") {
                contactFields.add(ContactField(it.id, "Comment", it.comment))
            }
        }
        contactFields.sortedBy { x -> x.fieldName }

        var countOfFields = contactFields.size + 2

        val linearLayout = EditActivityLinearLayout
        val params: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        params.setMargins(50, 20, 50, 20)

        val viewFN = TextInputLayout(this)
        val viewFN2 = TextInputEditText(this)

        viewFN2.setText(contactToEdit.firsname)
        viewFN2.hint = "First name"
        viewFN2.tag = contactToEdit.id
        viewFN.addView(viewFN2)
        viewFN.layoutParams = params
        linearLayout.addView(viewFN)

        val viewLN = TextInputLayout(this)
        val viewLN2 = TextInputEditText(this)

        viewLN2.setText(contactToEdit.lastname)
        viewLN2.hint = "Last name"
        viewLN2.tag = contactToEdit.id
        viewLN.addView(viewLN2)
        viewLN.layoutParams = params;
        linearLayout.addView(viewLN)


        contactFields.forEach {
            val view = TextInputLayout(this)
            val view2 = TextInputEditText(this)

            view2.setText(it.valueName)
            view2.hint = it.fieldName
            view2.tag = it.contactId
            view.addView(view2)
            view.layoutParams = params;
            linearLayout.addView(view)
        }

    }


    fun buttonBackOnClick(view: View) {
        contactsRepo.close()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun buttonDeleteOnClick(view: View) {

        contactsRepo.deleteByID(contactToEdit.id)

        (adapter as DataRecycleViewAdapterAllContacts).refreshData()
        adapter.notifyDataSetChanged()

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)

    }

    fun buttonEditOnClick(view: View) {
        for (i in 0 until EditActivityLinearLayout.childCount) {
            var view = EditActivityLinearLayout.getChildAt(i);
            if (view is TextInputLayout) {
                if (view.editText?.hint == "First name") {
                    contactsRepo.editByID(
                        DbHelper.PERSONS_TABLE_NAME,
                        DbHelper.PERSON_FIRSTNAME,
                        view.editText?.text.toString(),
                        view.editText?.tag as Int
                    )
                }
                if (view.editText?.hint == "Last name") {
                    contactsRepo.editByID(
                        DbHelper.PERSONS_TABLE_NAME,
                        DbHelper.PERSON_LASTNAME,
                        view.editText?.text.toString(),
                        view.editText?.tag as Int
                    )
                }
                if (view.editText?.hint == "Phone number") {
                    contactsRepo.editByID(
                        DbHelper.CONTACTS_TABLE_NAME,
                        DbHelper.CONTACT_PHONE,
                        view.editText?.text.toString(),
                        view.editText?.tag as Int
                    )
                }
                if (view.editText?.hint == "E-mail") {
                    contactsRepo.editByID(
                        DbHelper.CONTACTS_TABLE_NAME,
                        DbHelper.CONTACT_EMAIL,
                        view.editText?.text.toString(),
                        view.editText?.tag as Int
                    )
                }
                if (view.editText?.hint == "Skype") {
                    contactsRepo.editByID(
                        DbHelper.CONTACTS_TABLE_NAME,
                        DbHelper.CONTACT_SKYPE,
                        view.editText?.text.toString(),
                        view.editText?.tag as Int
                    )
                }
                if (view.editText?.hint == "Address") {
                    contactsRepo.editByID(
                        DbHelper.CONTACTS_TABLE_NAME,
                        DbHelper.CONTACT_ADDRESS,
                        view.editText?.text.toString(),
                        view.editText?.tag as Int
                    )
                }
                if (view.editText?.hint == "Comment") {
                    contactsRepo.editByID(
                        DbHelper.CONTACTS_TABLE_NAME,
                        DbHelper.CONTACT_COMMENT,
                        view.editText?.text.toString(),
                        view.editText?.tag as Int
                    )
                }
            }
        }

        (adapter as DataRecycleViewAdapterAllContacts).refreshData()
        adapter.notifyDataSetChanged()

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun addFieldsButtonOnClick(view: View) {
        val intent = Intent(this, AddFieldActivity::class.java)
        intent.putExtra("selectedContactID", contactToEdit.id)
        startActivity(intent)
    }
}

package com.example.mycontacts

class Contact {
    var id: Int = 0;
    var personId: Int = 0;
    var phone: String = ""
    var email: String = ""
    var skype: String = ""
    var address: String = ""
    var comment: String = ""

    constructor(personId: Int, phone: String, email: String, skype: String, address: String, comment: String)
            : this(0, personId, phone, email, skype, address, comment)

    constructor(id: Int, personId: Int, phone: String, email: String, skype: String, address: String, comment: String) {
        this.id = id
        this.personId = personId
        this.phone = phone
        this.email = email
        this.skype = skype
        this.address = address
        this.comment = comment
    }

}
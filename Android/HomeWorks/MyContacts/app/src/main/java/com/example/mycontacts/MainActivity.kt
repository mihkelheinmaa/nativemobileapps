package com.example.mycontacts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.os.RecoverySystem
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : DataRecycleViewAdapterAllContacts.OnContactListener, AppCompatActivity()  {


    private lateinit var contactsRepo: ContactRepository
    private lateinit var adapter: RecyclerView.Adapter<*>
    private lateinit var dataSet: List<Person>
    private var savedPosition: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        contactsRepo = ContactRepository(this).open()

        AllContactsView.layoutManager = LinearLayoutManager(this)
        adapter = DataRecycleViewAdapterAllContacts(this, contactsRepo, this)
        AllContactsView.adapter = adapter
        dataSet = (adapter as DataRecycleViewAdapterAllContacts).getDataInList()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        savedPosition?.let { outState.putInt("adapterPosition", it) }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        savedPosition = savedInstanceState.getInt("adapterPosition")
    }

    override fun onResume() {
        super.onResume()
        savedPosition?.let { onContactClick(it) }
    }

    fun AddNewOnClick(view: View) {
        val intent = Intent(this, AddContactActivity::class.java)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        contactsRepo.close()
    }

    override fun onContactClick(position: Int) {

        savedPosition = position
        SelectedContactName.text = dataSet[position].firsname + " " + dataSet[position].lastname.toUpperCase(
            Locale.ROOT)

        SelectedContactContacts.layoutManager = LinearLayoutManager(this)
        adapter = DataRecycleViewAdapterContact(this, dataSet[position], contactsRepo)
        SelectedContactContacts.adapter = adapter
    }

    fun editContactButtonOnClick(view: View) {
        if (savedPosition != null) {
            val intent = Intent(this, EditContactActivity::class.java)
            intent.putExtra("selectedContactID", dataSet[this.savedPosition!!].id)
            startActivity(intent)
        } else {
            SelectedContactName.text = "Please first choose a contact"
        }
    }
}
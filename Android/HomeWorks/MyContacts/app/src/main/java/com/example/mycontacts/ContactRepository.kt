package com.example.mycontacts

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log

class ContactRepository(val context: Context) {

    private lateinit var dbHelper: DbHelper
    private lateinit var db: SQLiteDatabase

    fun open(): ContactRepository {
        dbHelper = DbHelper(context)
        db = dbHelper.writableDatabase
        return this
    }

    fun close() {
        dbHelper.close()
    }

    fun addPerson(person: Person) : Int {
        val cv = ContentValues()

        cv.put(DbHelper.PERSON_FIRSTNAME, person.firsname)
        cv.put(DbHelper.PERSON_LASTNAME, person.lastname)

        var lastInsertedId = db.insert(DbHelper.PERSONS_TABLE_NAME, null, cv)
        return lastInsertedId.toInt()
    }

    fun addContact(contact: Contact) {
        val contentValues = ContentValues()

        contentValues.put(DbHelper.PERSON_FK, contact.personId)
        contentValues.put(DbHelper.CONTACT_PHONE, contact.phone)
        contentValues.put(DbHelper.CONTACT_EMAIL, contact.email)
        contentValues.put(DbHelper.CONTACT_SKYPE, contact.skype)
        contentValues.put(DbHelper.CONTACT_ADDRESS, contact.address)
        contentValues.put(DbHelper.CONTACT_COMMENT, contact.comment)

        db.insert(DbHelper.CONTACTS_TABLE_NAME, null, contentValues)
    }

    fun getAllPersons() : List<Person> {
        val persons = ArrayList<Person>()
        val columns = arrayOf(
            DbHelper.PERSON_ID,
            DbHelper.PERSON_FIRSTNAME,
            DbHelper.PERSON_LASTNAME
        )

        val cursor = db.query(DbHelper.PERSONS_TABLE_NAME, columns, null, null, null, null, null)

        while (cursor.moveToNext()) {
            persons.add(
                Person(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2)
                )
            )
        }
        cursor.close()
        return persons
    }


    fun getAllContactsForPerson(person: Person): List<Contact> {
        val contacts = ArrayList<Contact>()

        val columns = arrayOf(
            DbHelper.CONTACT_ID,
            DbHelper.PERSON_FK,
            DbHelper.CONTACT_PHONE,
            DbHelper.CONTACT_EMAIL,
            DbHelper.CONTACT_SKYPE,
            DbHelper.CONTACT_ADDRESS,
            DbHelper.CONTACT_COMMENT
        )

//        val where = "${DbHelper.PERSON_FK} = ${person.id}"

//        val cursor = db.query(DbHelper.CONTACTS_TABLE_NAME, columns, null, null, null, null, null)
        val cursor = db.query(DbHelper.CONTACTS_TABLE_NAME, columns, DbHelper.PERSON_FK + " = " + person.id, null, null, null, null)
//        val cursor = db.rawQuery("SELECT * FROM " + DbHelper.CONTACTS_TABLE_NAME + " WHERE " + DbHelper.PERSON_FK + " = " + person.id, null)

        while (cursor.moveToNext()) {
            contacts.add(
                Contact(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6)
                )
            )
        }
        cursor.close()
        return contacts
    }

    fun getPersonByID(keyID : Int): Person {
        val columns = arrayOf(
            DbHelper.PERSON_ID,
            DbHelper.PERSON_FIRSTNAME,
            DbHelper.PERSON_LASTNAME
        )

        val cursor = db.rawQuery("SELECT * FROM " + DbHelper.PERSONS_TABLE_NAME + " WHERE " + DbHelper.PERSON_ID + " = " + keyID, null)

        cursor.moveToFirst()
        val person = Person(
            cursor.getInt(0),
            cursor.getString(1),
            cursor.getString(2)
        )

        cursor.close()
        return person
    }

    fun deleteByID(keyID: Int) {
        db.delete(DbHelper.PERSONS_TABLE_NAME, DbHelper.PERSON_ID + " = " + keyID, null)

    }

    fun editByID(table: String, field: String, value: String, id: Int) {

        val cv = ContentValues()
            cv.put(field, value)

        if (table == DbHelper.CONTACTS_TABLE_NAME) {
            db.update(DbHelper.CONTACTS_TABLE_NAME, cv, DbHelper.CONTACT_ID + " = " + id, null)
        } else if (table == DbHelper.PERSONS_TABLE_NAME) {
            db.update(DbHelper.PERSONS_TABLE_NAME, cv, DbHelper.PERSON_ID + " = " + id, null)
        }

    }

}
package com.example.mycontacts

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class DbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        const val DATABASE_NAME = "contacts.db"
        const val DATABASE_VERSION = 1


        const val PERSONS_TABLE_NAME = "PERSONS"
        const val PERSON_ID = "_id"
        const val PERSON_FIRSTNAME = "firstname"
        const val PERSON_LASTNAME = "lastname"

        const val SQL_PERSON_CREATE_TABLE =
            "create table $PERSONS_TABLE_NAME (" +
                    "$PERSON_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "$PERSON_FIRSTNAME," +
                    "$PERSON_LASTNAME);"



        const val CONTACTS_TABLE_NAME = "CONTACTS"
        const val CONTACT_ID = "_id"
        const val PERSON_FK = "person_id"
        const val CONTACT_PHONE = "phone"
        const val CONTACT_EMAIL = "email"
        const val CONTACT_SKYPE = "skype"
        const val CONTACT_ADDRESS = "address"
        const val CONTACT_COMMENT = "comment"

        const val SQL_CONTACT_CREATE_TABLE =
            "create table $CONTACTS_TABLE_NAME (" +
                    "$CONTACT_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "$PERSON_FK," +
                    "$CONTACT_PHONE," +
                    "$CONTACT_EMAIL," +
                    "$CONTACT_SKYPE," +
                    "$CONTACT_ADDRESS," +
                    "$CONTACT_COMMENT);"



        const val SQL_DELETE_CONTACTS_TABLE = "DROP TABLE IF EXISTS $CONTACTS_TABLE_NAME;"
        const val SQL_DELETE_PERSONS_TABLE = "DROP TABLE IF EXISTS $PERSONS_TABLE_NAME;"

    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CONTACT_CREATE_TABLE)
//        Log.d("BAAS", "CONTACTS created")
        db?.execSQL(SQL_PERSON_CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(SQL_DELETE_CONTACTS_TABLE)
        db?.execSQL(SQL_DELETE_PERSONS_TABLE)
        onCreate(db)
    }
    
}
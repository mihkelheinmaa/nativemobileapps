package com.example.mycontacts

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.add_contact.*
import kotlinx.android.synthetic.main.add_field.*
import kotlinx.android.synthetic.main.add_field.contactAddress
import kotlinx.android.synthetic.main.add_field.contactComment
import kotlinx.android.synthetic.main.add_field.contactPhone
import kotlinx.android.synthetic.main.add_field.contactSkype
import kotlinx.android.synthetic.main.add_field.contacteMail

class AddFieldActivity : AppCompatActivity() {

    private lateinit var contactsRepo: ContactRepository
    private lateinit var adapter: RecyclerView.Adapter<*>
    lateinit var contactToEdit: Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_field)

        contactsRepo = ContactRepository(this).open()
        adapter = DataRecycleViewAdapterAllContacts(this, contactsRepo, MainActivity())

        var keyID = intent.getIntExtra("selectedContactID", 0)
        contactToEdit = contactsRepo.getPersonByID(keyID)

        contactToEditTextView.text = contactToEdit.firsname.toUpperCase() + " " + contactToEdit.lastname.toUpperCase()
    }

    fun buttonSaveOnClick(@Suppress("UNUSED_PARAMETER")view: View) {

        contactsRepo.addContact(
            Contact(
                contactToEdit.id,
                contactPhone.text.toString(),
                contacteMail.text.toString(),
                contactSkype.text.toString(),
                contactAddress.text.toString(),
                contactComment.text.toString()
            )
        )

        (adapter as DataRecycleViewAdapterAllContacts).refreshData()
        adapter.notifyDataSetChanged()

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)

    }

    fun buttonBackOnClick(@Suppress("UNUSED_PARAMETER")view: View) {
        val intent = Intent(this, EditContactActivity::class.java)
        intent.putExtra("selectedContactID", contactToEdit.id)
        startActivity(intent)
    }


}
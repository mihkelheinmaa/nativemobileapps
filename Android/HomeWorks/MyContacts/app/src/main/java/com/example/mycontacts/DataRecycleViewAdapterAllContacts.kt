package com.example.mycontacts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_view_all_contacts.view.*
import java.util.*

class DataRecycleViewAdapterAllContacts(private val context: Context, private val repo: ContactRepository, private val onContactListener1: OnContactListener)
    : RecyclerView.Adapter<DataRecycleViewAdapterAllContacts.ViewHolder>() {

    lateinit var dataSet: List<Person>
    lateinit var mOnContactListener : OnContactListener

    lateinit var itemView: View

    fun refreshData() {
        dataSet = repo.getAllPersons().sortedBy { x -> x.firsname}

        mOnContactListener = onContactListener1
    }

    init {
        refreshData()
    }

    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rowView = inflater.inflate(R.layout.row_view_all_contacts, parent, false)
        itemView = rowView
        return ViewHolder(rowView, mOnContactListener)
    }

    override fun getItemCount(): Int {
        return dataSet.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = dataSet[position]
        holder.itemView.ContactID.text = contact.firsname[0].toString().toUpperCase(Locale.ROOT)
        holder.itemView.ContactFirstName.text = contact.firsname
        holder.itemView.ContactLastName.text = contact.lastname.toUpperCase(Locale.ROOT)
    }

    fun getDataInList(): List<Person> {
        return dataSet
    }

    interface OnContactListener {
        fun onContactClick(position: Int)
    }

    inner class ViewHolder() : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        lateinit var onContactListener: OnContactListener
        lateinit var itemView2 : View

        constructor(itemView: View, clickListener: OnContactListener) : this() {
            this.onContactListener = clickListener
            this.itemView2 = itemView
            itemView2.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onContactListener.onContactClick(adapterPosition)
        }
    }
}
package com.example.mycontacts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.add_contact.*
import kotlinx.android.synthetic.main.row_view_all_contacts.*

class AddContactActivity : AppCompatActivity() {

    private lateinit var contactsRepo: ContactRepository
    private lateinit var adapter: RecyclerView.Adapter<*>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_contact)

        contactsRepo = ContactRepository(this).open()
        adapter = DataRecycleViewAdapterAllContacts(this, contactsRepo, MainActivity())
    }

    fun buttonSaveOnClick(view: View) {

        var personId = contactsRepo.addPerson(
            Person(
                contactFirstName.text.toString(),
                contactLastName.text.toString()
            )
        )

        contactsRepo.addContact(
            Contact(
                personId,
                contactPhone.text.toString(),
                contacteMail.text.toString(),
                contactSkype.text.toString(),
                contactAddress.text.toString(),
                contactComment.text.toString()
            )
        )

        (adapter as DataRecycleViewAdapterAllContacts).refreshData()
        adapter.notifyDataSetChanged()

        val intent = Intent(this, AddContactActivity::class.java)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        contactsRepo.close()
    }

    fun buttonBackOnClick(view: View) {
        contactsRepo.close()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}
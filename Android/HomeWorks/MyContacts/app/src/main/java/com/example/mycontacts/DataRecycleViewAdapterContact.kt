package com.example.mycontacts

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_view_current_contact.view.*

class DataRecycleViewAdapterContact(private val context: Context, private val person: Person, private val repo: ContactRepository)
    : RecyclerView.Adapter<DataRecycleViewAdapterContact.ViewHolder>() {

    lateinit var mPerson: Person
    lateinit var mRepo: ContactRepository
    lateinit var contactFields: List<ContactField>

    fun refreshData() {
        mPerson = person
        mRepo = repo
        contactFields = getContacts()
//            arrayListOf<String>(mContact.firsname, mContact.lastname, mContact.phone, mContact.email, mContact.skype, mContact.address, mContact.comment)
    }

    init {
        refreshData()
    }

    private val inflater = LayoutInflater.from(context)

    fun getContacts(): List<ContactField> {
        var contacts = repo.getAllContactsForPerson(person)
        val contactFields = ArrayList<ContactField>()

        contacts.forEach {
            if (it.phone != "") {
                contactFields.add(ContactField(it.id, "Phone number", it.phone))
            }
            if (it.email != "") {
                contactFields.add(ContactField(it.id, "E-mail", it.email))
            }
            if (it.skype != "") {
                contactFields.add(ContactField(it.id, "Skype", it.skype))
            }
            if (it.address != "") {
                contactFields.add(ContactField(it.id, "Address", it.address))
            }
            if (it.comment != "") {
                contactFields.add(ContactField(it.id, "Comment", it.comment))
            }
        }
        return contactFields.sortedBy { x -> x.fieldName }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rowView = inflater.inflate(R.layout.row_view_current_contact, parent, false)
        return ViewHolder(rowView)
    }

    override fun getItemCount(): Int {
        return contactFields.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            var contactLine = contactFields[position]
            holder.itemView.entryName.text = contactLine.fieldName
            holder.itemView.entryValue.text = contactLine.valueName
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}

package ee.taltech.connect4

import android.content.Intent
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class GameEndActivity : AppCompatActivity() {

    var nextMoveByRed : Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_end)

        findViewById<Button>(R.id.EndActivityReturnButton).setOnClickListener { returnButtonOnClick() }

        val intent : Intent = intent
        nextMoveByRed = intent.getBooleanExtra("nextMoveByRed", true)
        val noWinner = intent.getBooleanExtra("noWinner", true)

        if(!noWinner) {
            checkWhoWon()
            val winnerIcon: ImageView = findViewById(R.id.winnerIcon)
            winnerIcon.startAnimation(
                AnimationUtils.loadAnimation(
                    applicationContext,
                    R.anim.bounce
                )
            )
        }
    }

    private fun checkWhoWon() {
        val infoText : TextView
        val winnerIcon : ImageView
        val id1 = resources.getIdentifier("winnerIcon", "id", packageName)
        val id2 = resources.getIdentifier("EndViewInfo", "id", packageName)
        infoText = findViewById(id2)
        winnerIcon = findViewById(id1)
        if (!nextMoveByRed) {
            infoText.text = getString(R.string.yellow_won)
            winnerIcon.setImageResource(R.drawable.circle_yellow)
            winnerIcon.alpha = 1.0F
        } else if (nextMoveByRed) {
            infoText.text = getString(R.string.red_won)
            winnerIcon.setImageResource(R.drawable.circle_red)
            winnerIcon.alpha = 1.0F
        }
    }

    private fun returnButtonOnClick() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

}
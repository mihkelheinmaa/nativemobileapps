package ee.taltech.connect4

import java.io.Serializable

class Tile(color: String) : Serializable {
    var color: String? = color
    var winningTile = false

}
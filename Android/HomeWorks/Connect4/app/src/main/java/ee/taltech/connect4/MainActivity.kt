package ee.taltech.connect4

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Chronometer
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.stats.*


class MainActivity : AppCompatActivity() {

    companion object {
        private val TAG = this::class.java.declaringClass!!.simpleName
    }

    private var game: Connect4 = Connect4()
    private var gameSlots: ArrayList<ImageButton> = arrayListOf()

    private var buttonRed: Int = R.drawable.circle_red
    private var buttonYellow: Int = R.drawable.circle_yellow

    private lateinit var restartText: TextView
    private lateinit var nextMoveByWhoField: ImageView
    private lateinit var availableSlotsField: TextView
    private lateinit var turnsUsedField: TextView

    private lateinit var stopwatch: Chronometer


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        Log.d(TAG, "onCreate")

        getBoardSlots()
        restartText = findViewById(R.id.restartText)
        nextMoveByWhoField = findViewById(R.id.nextTurnData)
        availableSlotsField = findViewById(R.id.slotsAvailableData)
        turnsUsedField = findViewById(R.id.turnsUsedData)
        stopwatch = findViewById(R.id.timeElapsedData)

        findViewById<ImageButton>(R.id.restartButton).setOnClickListener { restartButtonOnClick() }

        if (savedInstanceState != null) {
            restoreGameState(savedInstanceState)
        } else {
            stopwatch.start()
        }

        val restartButton: ImageButton = findViewById(R.id.restartButton)
        restartButton.startAnimation(
            AnimationUtils.loadAnimation(
                applicationContext,
                R.anim.infinite_fading
            )
        )
        val restartText: TextView = findViewById(R.id.restartText)
        restartText.startAnimation(
            AnimationUtils.loadAnimation(
                applicationContext,
                R.anim.slide_in
            )
        )
    }

    @Suppress("UNCHECKED_CAST")
    private fun restoreGameState(savedInstanceState: Bundle) {
        game.board = savedInstanceState.getSerializable("serializedGameBoard") as Array<Array<Tile>>
        game.nextMoveByRed = savedInstanceState.getBoolean("serializedGameWhoseTurn")
        game.slotsAvailable = savedInstanceState.getInt("serializedGameAvailableSlots")
        game.turnsUsed = savedInstanceState.getInt("serializedGameTurnsUsed")
        game.won = savedInstanceState.getBoolean("serializedGameIsWon")
        updateGameState(0, true)
        stopwatch.base = savedInstanceState.getLong("stopWatchBase")
        stopwatch.start()
    }

    private fun getBoardSlots() {
        var view: ImageButton
        for (i in 1..7) {
            for (j in 1..8) {
                val ids = "r" + i + "c" + j
                val id = resources.getIdentifier(ids, "id", packageName)
                if (id != 0) {
                    view = findViewById(id)
                    gameSlots.add(view)
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d(TAG, "onSaveInstanceState")

        outState.putLong("stopWatchBase", stopwatch.base)
        outState.putSerializable("serializedGameBoard", game.board)
        outState.putBoolean("serializedGameWhoseTurn", game.nextMoveByRed)
        outState.putInt("serializedGameAvailableSlots", game.slotsAvailable)
        outState.putInt("serializedGameTurnsUsed", game.turnsUsed)
        outState.putBoolean("serializedGameIsWon", game.won)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        Log.d(TAG, "onRestoreInstanceState")

    }

    override fun onResume() {
        super.onResume()

        Log.d(TAG, "onResume")
    }


    fun gameButtonOnClick(view: View) {
        val restartText: TextView = findViewById(R.id.restartText)
        restartText.text = getString(R.string.textRestart)

        val (col, row) = getColRow((view.tag as String).toInt())

        when (game.move(col)) {
            "columnFull" -> {
                restartText.text = getString(R.string.textTryAgain)
                updateGameState(col, false)
            }
            "win" -> {
                stopwatch.stop()
                updateGameState(col, false)
                reDrawWinnerBoard()
                game.won = true
                sendIntentEndActivity(false)
            }
            else -> {
                updateGameState(col, false)
                restartText.text = getString(R.string.textRestart)}
        }

        if (game.slotsAvailable == 0) {
            sendIntentEndActivity(true)
        }
    }

    private fun updateGameState(mCol: Int, updateAll: Boolean) {
        gameSlots.forEach {
            val (col, row) = getColRow((it.tag as String).toInt())
            if (updateAll) {
                val tile = game.getTile(col, row)
                if (tile.color == "red") {
                    it.setImageResource(buttonRed)
                } else if (tile.color == "yellow") {
                    it.setImageResource(buttonYellow)
                }
            } else if (col == mCol && !updateAll) {
                val tile = game.getTile(col, row)
                if (tile.color == "red") {
                    it.setImageResource(buttonRed)
                } else if (tile.color == "yellow") {
                    it.setImageResource(buttonYellow)
                }
            }
        }
        if (game.nextMoveByRed) {
            nextMoveByWhoField.setImageResource(buttonRed)
        } else {
            nextMoveByWhoField.setImageResource(buttonYellow)
        }
        turnsUsedField.text = game.turnsUsed.toString()
        availableSlotsField.text = game.slotsAvailable.toString()
    }

    private fun reDrawWinnerBoard() {
        gameSlots.forEach {
            val (col, row) = getColRow((it.tag as String).toInt())
            val tile = game.getTile(col, row)
            if (!tile.winningTile) {
                if (tile.color != null) {
                    it.alpha = 0.2F
                }
            }
        }
    }

    private fun getColRow(tag: Int): Pair<Int, Int>{
        val mRow = tag / 7
        val mCol = tag - mRow * 7

        return Pair(mCol, mRow)
    }

    private fun restartButtonOnClick() {
        game.reset()
        gameSlots.forEach {
            it.setImageResource(R.drawable.circle_gray)
            it.alpha = 1F
        }
        nextMoveByWhoField.setImageResource(buttonRed)
        turnsUsedLabel.text = game.turnsUsed.toString()
        availableSlotsField.text = game.slotsAvailable.toString()

        stopwatch.stop()
        stopwatch.base = SystemClock.elapsedRealtime()
        stopwatch.start()

    }

    private fun sendIntentEndActivity(noWinner : Boolean) {
        val intent = Intent(this, GameEndActivity::class.java)
        intent.putExtra("nextMoveByRed", game.nextMoveByRed)
        intent.putExtra("noWinner", noWinner)
        GameEndActivity().nextMoveByRed = game.nextMoveByRed
        startActivity(intent)
    }
}
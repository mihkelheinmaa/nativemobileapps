package ee.taltech.connect4

class Connect4 {
    var won = false
    var board = Array(7){Array(6){ Tile("none") }}
    var nextMoveByRed = true
    var slotsAvailable = 42
    var turnsUsed = 0

    private fun getFirstUnusedTile(col: Int): Int {
        for (i in 5 downTo 0) {
            if (board[col][i].color == "none") {
                return i
            }
        }
        return -1
    }

    fun getTile(col: Int, row: Int): Tile {
        return board[col][row]
    }

    fun move(col: Int): String {
        val newRow = getFirstUnusedTile(col)
        if (newRow != -1) {
            if (nextMoveByRed) {
                board[col][newRow].color = "red"
            } else {
                board[col][newRow].color = "yellow"
            }
            if (!nextMoveByRed) {
                turnsUsed != 1
            }
            slotsAvailable -= 1

            if (checkForWin(col, newRow)) {
                return "win"
            }

            nextMoveByRed = !nextMoveByRed
            return "next"
        }
        return "columnFull"
    }

    private fun checkForWin(col: Int, row: Int): Boolean {
        var color = "yellow"
        if (nextMoveByRed) {
            color = "red"
        }

        if (checkForWinHorizontally(row, color)) {
            return true
        }
        if (checkForWinVertically(col, color)) {
            return true
        }
        if (checkForWinDiagonally(col, row, color)) {
            return true
        }

        return false
    }

    private fun checkForWinHorizontally(row: Int, color: String): Boolean {
        var count = 0
        var best = 0
        val winningTilesTemp: ArrayList<ArrayList<Int>> = arrayListOf()
        var winningTiles: ArrayList<ArrayList<Int>> = arrayListOf()

        for (i in 0..6) {
            if (board[i][row].color != color) {
                if (count > best) {
                    best = count
                    winningTiles = winningTilesTemp
                }
                count = 0
                winningTilesTemp.clear()
            } else {
                count += 1
                winningTilesTemp.add(arrayListOf(i, row))
            }
        }
        if (best >= 4 || count >= 4) {
            if (winningTiles.size < winningTilesTemp.size) {
                winningTiles = winningTilesTemp
            }
            getWinningTiles(winningTiles)
            return true
        }
        return false
    }

    private fun checkForWinVertically(col: Int, color: String): Boolean {
        var count = 0
        var best = 0
        val winningTilesTemp: ArrayList<ArrayList<Int>> = arrayListOf()
        var winningTiles: ArrayList<ArrayList<Int>> = arrayListOf()

        for (i in 0..5) {
            if (board[col][i].color != color) {
                if (count > best) {
                    best = count
                    winningTiles = winningTilesTemp
                }
                count = 0
                winningTilesTemp.clear()
            } else {
                count += 1
                winningTilesTemp.add(arrayListOf(col, i))
            }
        }
        if (best >= 4 || count >= 4) {
            if (winningTiles.size < winningTilesTemp.size) {
                winningTiles = winningTilesTemp
            }
            getWinningTiles(winningTiles)
            return true
        }
        return false
    }

    private fun checkForWinDiagonally(col: Int, row: Int, color: String): Boolean {
        val matchingTilesNW: ArrayList<ArrayList<Int>> = arrayListOf()
        val matchingTilesNE: ArrayList<ArrayList<Int>> = arrayListOf()
        val matchingTilesSE: ArrayList<ArrayList<Int>> = arrayListOf()
        val matchingTilesSW: ArrayList<ArrayList<Int>> = arrayListOf()

        var mCol = col
        var mRow = row

        do {
            mCol += 1
            if (mCol == 7) {
                break
            }
            mRow += 1
            if (mRow == 6) {
                break
            }
            if (board[mCol][mRow].color == color) {
                matchingTilesSE.add(arrayListOf(mCol, mRow))
            } else {
                break
            }
        } while (board[mCol][mRow].color == color)

        mCol = col
        mRow = row

        do {
            mCol += 1
            if (mCol == 7) {
                break
            }
            mRow -= 1
            if (mRow == -1) {
                break
            }
            if (board[mCol][mRow].color == color) {
                matchingTilesNE.add(arrayListOf(mCol, mRow))
            } else {
                break
            }
        } while (board[mCol][mRow].color == color)

        mCol = col
        mRow = row

        do {
            mCol -= 1
            if (mCol == -1) {
                break
            }
            mRow -= 1
            if (mRow == -1) {
                break
            }
            if (board[mCol][mRow].color == color) {
                matchingTilesNW.add(arrayListOf(mCol, mRow))
            } else {
                break
            }
        } while (board[mCol][mRow].color == color)

        mCol = col
        mRow = row

        do {
            mCol -= 1
            if (mCol == -1) {
                break
            }
            mRow += 1
            if (mRow == 6) {
                break
            }
            if (board[mCol][mRow].color == color) {
                matchingTilesSW.add(arrayListOf(mCol, mRow))
            } else {
                break
            }
        } while (board[mCol][mRow].color == color)

        if (matchingTilesNE.size + matchingTilesSW.size >= 3) {
            matchingTilesNE.addAll(matchingTilesSW)
            matchingTilesNE.add(arrayListOf(col, row))
            getWinningTiles(matchingTilesNE)
            return true
        }
        if (matchingTilesNW.size + matchingTilesSE.size >= 3) {
            matchingTilesNW.addAll(matchingTilesSE)
            matchingTilesNW.add(arrayListOf(col, row))
            getWinningTiles(matchingTilesNW)
            return true
        }
        return false
    }

    private fun getWinningTiles(tiles: ArrayList<ArrayList<Int>>) {
        tiles.forEach {
            board[it[0]][it[1]].winningTile = true
        }
    }

    fun reset() {
        board = Array(7){Array(6){ Tile("none") }}
        nextMoveByRed = true
        slotsAvailable = 42
        turnsUsed = 0
        won = false
    }
}

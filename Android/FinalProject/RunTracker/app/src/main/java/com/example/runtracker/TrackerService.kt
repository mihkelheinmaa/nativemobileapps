package com.example.runtracker

import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.os.SystemClock
import android.util.Log
import android.widget.RemoteViews
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.runtracker.domain.GpxPoint
import com.google.android.gms.location.*
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.json.JSONObject
import java.net.InetAddress
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap


class TrackerService : Service() {
    companion object {
        val TAG: String = this::class.java.declaringClass!!.simpleName!!
    }

    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 2000
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2

    private val broadcastReceiver = InnerBroadcastReceiver()
    private val broadcastReceiverIntentFilter: IntentFilter = IntentFilter()

    private val mLocationRequest: LocationRequest = LocationRequest()
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private var mLocationCallback: LocationCallback? = null

    private var currentLocation: Location? = null

    private val startTimeStamp = System.currentTimeMillis()
    private var wpStartTimeStamp = System.currentTimeMillis()
    private var cpStartTimeStamp = System.currentTimeMillis()

    private var lastWPTimerBase = System.currentTimeMillis()
    private var lastCPTimerBase = System.currentTimeMillis()

    private var locationStart: Location? = null
    private var distanceOverallTotal = 0f
    private var pace = 0f

    private var locationCP: Location? = null
    private var distanceCPDirect = 0f
    private var distanceCPTotal = 0f

    private var locationWP: Location? = null
    private var distanceWPDirect = 0f
    private var distanceWPTotal = 0f


    private var hasStarted = true
    private var firstName = ""
    private var lastName = ""
    private var email = ""

    private lateinit var notifyview : RemoteViews

    private var token = ""
    private var sessionId = ""
    private var locId: String = "00000000-0000-0000-0000-000000000001"
    private var wpId: String = "00000000-0000-0000-0000-000000000002"
    private var cpId: String = "00000000-0000-0000-0000-000000000003"

    private var wpMarkerCoordinates: ArrayList<Double> = ArrayList()
    private var cpMarkers: ArrayList<ArrayList<Double>> = ArrayList()
    private var allLocations: ArrayList<ArrayList<Double>> = ArrayList()

    private var isRunning = true
//    private var gpsPoints: ArrayList<GpxPoint> = ArrayList()
    private var reTryCounter = 0
    private var firstPoint = true




    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        Log.d(TAG, "onCreate")
        super.onCreate()

        broadcastReceiverIntentFilter.addAction(C.NOTIFICATION_ACTION_CP)
        broadcastReceiverIntentFilter.addAction(C.NOTIFICATION_ACTION_WP)
        broadcastReceiverIntentFilter.addAction(C.LOCATION_UPDATE_ACTION)
        broadcastReceiverIntentFilter.addAction(C.FINISHGPX)

        registerReceiver(broadcastReceiver, broadcastReceiverIntentFilter)


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                onNewLocation(locationResult.lastLocation)
            }
        }

        getLastLocation()

        createLocationRequest()
        requestLocationUpdates()

    }

    private fun requestLocationUpdates() {
        Log.i(TAG, "Requesting location updates")

        try {
            mFusedLocationClient.requestLocationUpdates(
                    mLocationRequest,
                    mLocationCallback, Looper.myLooper()
            )
        } catch (unlikely: SecurityException) {
            Log.e(
                    TAG,
                    "Lost location permission. Could not request updates. $unlikely"
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun onNewLocation(location: Location) {
        if (currentLocation == null){
            locationStart = location
            locationCP = location
            locationWP = location
        } else {
            val timeElapsedTotalMS = (System.currentTimeMillis() - startTimeStamp)
            distanceOverallTotal += location.distanceTo(currentLocation)
            pace = ((timeElapsedTotalMS/1000)/60) / (distanceOverallTotal/1000)

            distanceCPDirect = location.distanceTo(locationCP)
            distanceCPTotal += location.distanceTo(locationCP)
            locationCP = location

            distanceWPDirect = location.distanceTo(locationWP)
            distanceWPTotal += location.distanceTo(locationWP)
            locationWP = location
        }
        // save the location for calculations
        currentLocation = location

        sendLocationToBackEndService(locId)
        showNotification()
//        saveBackEndGeneralLocation(locId)

        allLocations.add(arrayListOf(currentLocation!!.latitude, currentLocation!!.longitude))

        // broadcast new location to UI
        var intent = Intent(C.LOCATION_UPDATE_ACTION)
        intent = fillLocationIntentWithExtras(intent)

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

        var mIntent = Intent(C.BE_PUT_NEW_GPSPOINT)

    }

    private fun createLocationRequest() {
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.maxWaitTime = UPDATE_INTERVAL_IN_MILLISECONDS
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun getLastLocation() {
        try {
            mFusedLocationClient.lastLocation
                    .addOnCompleteListener { task -> if (task.isSuccessful) {
                        Log.w(TAG, "task successful")
                        if (task.result != null){
                            onNewLocation(task.result!!)
                        }
                    } else {
                        Log.w(TAG, "Failed to get location." + task.exception)
                    }}
        } catch (unlikely: SecurityException) {
            Log.e(TAG, "Lost location permission.$unlikely")
        }
    }


    override fun onDestroy() {
        Log.d(TAG, "onDestroy")
        super.onDestroy()

        val timeElapsedTotalMS = (System.currentTimeMillis() - startTimeStamp)
        val duration = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(timeElapsedTotalMS),
                TimeUnit.MILLISECONDS.toMinutes(timeElapsedTotalMS) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeElapsedTotalMS)),
                TimeUnit.MILLISECONDS.toSeconds(timeElapsedTotalMS) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeElapsedTotalMS)))

        val mIntent = Intent(C.GPX_STOP_SESSION)
        mIntent.putExtra(C.LOCATION_UPDATE_LATITUDE, currentLocation!!.latitude)
        mIntent.putExtra(C.LOCATION_UPDATE_LONGITUDE, currentLocation!!.longitude)
        mIntent.putExtra(C.LOCATION_UPDATE_ALTITUDE, currentLocation!!.altitude)
        mIntent.putExtra(C.DISTANCE, "%.3f km".format(distanceOverallTotal / 1000))
        mIntent.putExtra(C.DURATION, duration)
        mIntent.putExtra(C.PACE, "%.2f".format(pace))

        LocalBroadcastManager.getInstance(this).sendBroadcast(mIntent)

        //stop location updates
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)

        // remove notifications
        NotificationManagerCompat.from(this).cancelAll()


        // don't forget to unregister brodcast receiver!!!!
        unregisterReceiver(broadcastReceiver)


        // broadcast stop to UI
        val intent = Intent(C.LOCATION_UPDATE_ACTION)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)


//        stopService(Intent(this, GpxService::class.java))

    }

    override fun onLowMemory() {
        Log.d(TAG, "onLowMemory")
        super.onLowMemory()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand")

        firstName = intent!!.getStringExtra(C.FIRST_NAME).toString()
        lastName = intent.getStringExtra(C.LAST_NAME).toString()
        email = intent.getStringExtra(C.EMAIL).toString()
        token = intent.getStringExtra(C.TOKEN).toString()

        var sIntent = Intent(this, DatabaseService::class.java)
        sIntent.putExtra(C.TOKEN, token)
        startService(sIntent)

        notifyview = RemoteViews(packageName, R.layout.bottom_controller)

        hasStarted = true

        // set counters and locations to 0/null
        currentLocation = null
        locationStart = null
        locationCP = null
        locationWP = null

        distanceOverallTotal = 0f
        distanceCPDirect = 0f
        distanceCPTotal = 0f
        distanceWPDirect = 0f
        distanceWPTotal = 0f

        notifyview.setChronometer(R.id.total_time_field, SystemClock.elapsedRealtime(), null, true)
        notifyview.setChronometer(R.id.cp_time_field, SystemClock.elapsedRealtime(), null, true)
        notifyview.setChronometer(R.id.wp_time_field, SystemClock.elapsedRealtime(), null, true)

        lastWPTimerBase = SystemClock.elapsedRealtime()
        lastCPTimerBase = SystemClock.elapsedRealtime()

        showNotification()

        val mIntent = Intent(this, GpxService::class.java)
//        mIntent.putExtra("personName", intent.getStringExtra("personName"))
        mIntent.putExtra("lat", intent.getDoubleExtra("lat", 0.0))
        mIntent.putExtra("lng", intent.getDoubleExtra("lng", 0.0))
        mIntent.putExtra(C.FIRST_NAME, firstName)
        mIntent.putExtra(C.LAST_NAME, lastName)
        mIntent.putExtra(C.EMAIL, email)

        startService(mIntent)

//        startNewBackEndSession()

        return START_STICKY
        //return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.d(TAG, "onBind")
        TODO("not implemented")
    }

    override fun onRebind(intent: Intent?) {
        Log.d(TAG, "onRebind")
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.d(TAG, "onUnbind")
        return super.onUnbind(intent)

    }

    fun showNotification(){
        val intentCp = Intent(C.NOTIFICATION_ACTION_CP)
        val intentWp = Intent(C.NOTIFICATION_ACTION_WP)
        val intentSS = Intent(this, TrackingActivity::class.java)
        intentSS.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP

        val pendingIntentCp = PendingIntent.getBroadcast(this, 0, intentCp, 0)
        val pendingIntentWp = PendingIntent.getBroadcast(this, 0, intentWp, 0)
        val pendingIntentSS = PendingIntent.getActivity(this, 1, intentSS, PendingIntent.FLAG_UPDATE_CURRENT)

        notifyview.setOnClickPendingIntent(R.id.button_cp, pendingIntentCp)
        notifyview.setOnClickPendingIntent(R.id.button_wp, pendingIntentWp)
        notifyview.setOnClickPendingIntent(R.id.button_start, pendingIntentSS)

        notifyview.setTextViewText(R.id.total_dist_field, "%.3f km".format(distanceOverallTotal / 1000))
        notifyview.setTextViewText(R.id.total_speed_field, "%.2f".format(pace))

        notifyview.setTextViewText(R.id.wp_distance_field, "%.3f km".format(distanceWPTotal / 1000))
        notifyview.setTextViewText(R.id.wp_dist_direct_field, "%.3f km".format(distanceWPDirect / 1000))

        notifyview.setTextViewText(R.id.cp_distance_field, "%.3f km".format(distanceCPTotal / 1000))
        notifyview.setTextViewText(R.id.cp_dist_direct_field, "%.3f km".format(distanceCPDirect / 1000))

        // construct and show notification
        val builder = NotificationCompat.Builder(applicationContext, C.NOTIFICATION_CHANNEL)
                .setSmallIcon(R.drawable.notificationicon)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setOngoing(true)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setStyle(NotificationCompat.DecoratedCustomViewStyle())

        builder.setCustomBigContentView(notifyview)

        // Super important, start as foreground service - ie android considers this as an active app. Need visual reminder - notification.
        // must be called within 5 secs after service starts.
        startForeground(C.NOTIFICATION_ID, builder.build())
    }

    fun fillLocationIntentWithExtras(intent: Intent) : Intent {
        intent.putExtra(C.LOCATION_UPDATE_LATITUDE, currentLocation!!.latitude)
        intent.putExtra(C.LOCATION_UPDATE_LONGITUDE, currentLocation!!.longitude)
        intent.putExtra(C.LOCATION_UPDATE_ALTITUDE, currentLocation!!.altitude)

        intent.putExtra(C.LOCATION_UPDATE_TOTAL_DISTANCE, "%.3f km".format(distanceOverallTotal / 1000))
        intent.putExtra(C.LOCATION_UPDATE_TOTAL_SPEED, "%.2f".format(pace))

        intent.putExtra(C.LOCATION_UPDATE_WP_DISTANCE_TOTAL, "%.3f km".format(distanceWPTotal / 1000))
        intent.putExtra(C.LOCATION_UPDATE_WP_DISTANCE_DIRECT, "%.3f km".format(distanceWPDirect / 1000))

        intent.putExtra(C.LOCATION_UPDATE_CP_DISTANCE_TOTAL, "%.3f km".format(distanceCPTotal / 1000))
        intent.putExtra(C.LOCATION_UPDATE_CP_DISTANCE_DIRECT, "%.3f km".format(distanceCPDirect / 1000))

        intent.putExtra(C.ONRESUME_WP_MARKER, wpMarkerCoordinates)
        intent.putExtra(C.ONRESUME_CP_MARKERS, cpMarkers)
        intent.putExtra(C.ONRESUME_GENERAL_MARKERS, allLocations)

        intent.putExtra(C.TIMER_WP_BASE, lastWPTimerBase)
        intent.putExtra(C.TIMER_CP_BASE, lastCPTimerBase)



        return intent
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun markForTermination() {


        val timeElapsedTotalMS = (System.currentTimeMillis() - startTimeStamp)
        val duration = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(timeElapsedTotalMS),
                TimeUnit.MILLISECONDS.toMinutes(timeElapsedTotalMS) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeElapsedTotalMS)),
                TimeUnit.MILLISECONDS.toSeconds(timeElapsedTotalMS) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeElapsedTotalMS)))

        val mIntent = Intent(C.GPX_STOP_SESSION)
        mIntent.putExtra(C.LOCATION_UPDATE_LATITUDE, currentLocation!!.latitude)
        mIntent.putExtra(C.LOCATION_UPDATE_LONGITUDE, currentLocation!!.longitude)
        mIntent.putExtra(C.LOCATION_UPDATE_ALTITUDE, currentLocation!!.altitude)
        mIntent.putExtra(C.DISTANCE, "%.3f km".format(distanceOverallTotal / 1000))
        mIntent.putExtra(C.DURATION, duration)
        mIntent.putExtra(C.PACE, "%.2f".format(pace))

        LocalBroadcastManager.getInstance(this).sendBroadcast(mIntent)

        onDestroy()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun sendLocationToBackEndService(type: String) {
        val newPoint = GpxPoint(SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.getDefault()).format(Date(currentLocation!!.time)),
                currentLocation!!.latitude,
                currentLocation!!.longitude,
                currentLocation!!.accuracy,
                currentLocation!!.altitude,
                currentLocation!!.verticalAccuracyMeters,
                type)

//        gpsPoints.add(newPoint)
        var mIntent = Intent(C.BE_PUT_NEW_GPSPOINT)
        mIntent.putExtra(C.SERIALIZED_GPS_POINT, newPoint)
        LocalBroadcastManager.getInstance(this).sendBroadcast(mIntent)

    }


    private inner class InnerBroadcastReceiver: BroadcastReceiver() {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.d(TAG, intent!!.action.toString())
            when(intent.action){
//                C.FINISHGPX -> {
//                    markForTermination()
//                }
                C.NOTIFICATION_ACTION_WP -> {
                    lastWPTimerBase = SystemClock.elapsedRealtime()
                    var mIntent = Intent(C.LOCATION_MARK_WP_ACTION)
                    mIntent = fillLocationIntentWithExtras(mIntent)
                    LocalBroadcastManager.getInstance(this@TrackerService).sendBroadcast(mIntent)
                    locationWP = currentLocation
                    distanceWPDirect = 0f
                    distanceWPTotal = 0f
                    wpStartTimeStamp = System.currentTimeMillis()
                    notifyview.setChronometer(R.id.wp_time_field, SystemClock.elapsedRealtime(), null, true)
                    showNotification()
//                    saveBackEndGeneralLocation(wpId)
                    sendLocationToBackEndService(wpId)

                    wpMarkerCoordinates = ArrayList<Double>()
                    wpMarkerCoordinates.add(currentLocation!!.latitude)
                    wpMarkerCoordinates.add(currentLocation!!.longitude)
                }
                C.NOTIFICATION_ACTION_CP -> {
                    lastCPTimerBase = SystemClock.elapsedRealtime()
                    var mIntentForMarking = Intent(C.LOCATION_MARK_CP_ACTION)
                    mIntentForMarking = fillLocationIntentWithExtras(mIntentForMarking)
                    LocalBroadcastManager.getInstance(this@TrackerService).sendBroadcast(mIntentForMarking)

                    val mIntent = Intent(C.GPX_PUT_NEW_WAYPOINT)
                    mIntent.putExtra(C.LOCATION_UPDATE_LATITUDE, currentLocation!!.latitude)
                    mIntent.putExtra(C.LOCATION_UPDATE_LONGITUDE, currentLocation!!.longitude)
                    mIntent.putExtra(C.LOCATION_UPDATE_ALTITUDE, currentLocation!!.altitude)
                    LocalBroadcastManager.getInstance(this@TrackerService).sendBroadcast(mIntent)

                    locationCP = currentLocation
                    distanceCPDirect = 0f
                    distanceCPTotal = 0f
                    cpStartTimeStamp = System.currentTimeMillis()
                    notifyview.setChronometer(R.id.cp_time_field, SystemClock.elapsedRealtime(), null, true)
                    showNotification()
//                    saveBackEndGeneralLocation(cpId)
                    sendLocationToBackEndService(cpId)

                    cpMarkers.add(arrayListOf(currentLocation!!.latitude, currentLocation!!.longitude))
                }

            }
        }

    }

}
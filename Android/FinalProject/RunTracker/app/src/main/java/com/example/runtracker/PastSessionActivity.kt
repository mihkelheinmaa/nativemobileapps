package com.example.runtracker

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.ImageButton
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.example.runtracker.dal.Repository
import com.example.runtracker.domain.Activity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import io.ticofab.androidgpxparser.parser.GPXParser
import io.ticofab.androidgpxparser.parser.domain.Gpx
import io.ticofab.androidgpxparser.parser.domain.TrackPoint
import io.ticofab.androidgpxparser.parser.domain.WayPoint
import java.io.File
import java.io.InputStream

class PastSessionActivity : AppCompatActivity(), OnMapReadyCallback {

    private var token : String = ""
    private var firstName : String = ""
    private var lastName : String = ""
    private var email : String = ""
    private var activity: Activity? = null
    private var repo: Repository? = null

    private lateinit var mMap: GoogleMap
    private var polylineOptions = PolylineOptions().width(10f).color(Color.GREEN)
    private var polyline: Polyline? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_past_session)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.pastMap) as SupportMapFragment
        mapFragment.getMapAsync(this)

        repo = Repository(application)

        token = intent.getStringExtra(C.TOKEN).toString()
        firstName = intent.getStringExtra(C.FIRST_NAME).toString()
        lastName = intent.getStringExtra(C.LAST_NAME).toString()
        email = intent.getStringExtra(C.EMAIL).toString()

        activity = repo!!.getActivityById(intent.getIntExtra("activityId", 0))

        if (activity != null) {
            findViewById<TextView>(R.id.history_map_saved_at_field).text = activity!!.getSavedAt().substring(0, 16)
            findViewById<TextView>(R.id.history_map_pace_field).text = activity!!.getPace()
            findViewById<TextView>(R.id.history_map_distance_field).text = activity!!.getDistance()
            findViewById<TextView>(R.id.history_map_duration_field).text = activity!!.getDuration()

        findViewById<ImageButton>(R.id.history_delete_button).setOnClickListener { historyMapDeleteOnClickAction() }
        findViewById<ImageButton>(R.id.history_return_button).setOnClickListener { historyMapReturnOnClickAction() }
        findViewById<ImageButton>(R.id.history_export_button).setOnClickListener { historyExportButtonOnClickAction() }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun historyExportButtonOnClickAction() {
        val file = File(this.filesDir, activity!!.getFilePath())

        val u1 = FileProvider.getUriForFile(this, applicationContext.packageName.toString() + ".provider", file)

        val sendIntent = Intent(Intent.ACTION_SEND)
        sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "RunTracker Activity ${activity!!.getSavedAt()}")
        sendIntent.putExtra(Intent.EXTRA_STREAM, u1)
        sendIntent.type = "text/gpx"
        startActivity(sendIntent)
    }

    private fun markCheckPoint(wayPoint: WayPoint) {
        val cpLatLng = LatLng(wayPoint.latitude, wayPoint.longitude)
        mMap.addMarker(MarkerOptions().position(cpLatLng).title(wayPoint.name).icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpointcp)))
    }

    private fun extendPolyline(trackPoint: TrackPoint) {
        val cpLatLng = LatLng(trackPoint.latitude, trackPoint.longitude)
        if (polyline != null) {
            polyline!!.remove()
        }
        polylineOptions.add(cpLatLng)
        polyline = mMap.addPolyline(polylineOptions)
    }

    private fun markFinishToTheMap(trackPoint: TrackPoint) {
        val cpLatLng = LatLng(trackPoint.latitude, trackPoint.longitude)
        mMap.addMarker(MarkerOptions().position(cpLatLng).title("FINISH").icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpointfinish)))
    }

    private fun markStartToTheMap(trackPoint: TrackPoint) {
        val cpLatLng = LatLng(trackPoint.latitude, trackPoint.longitude)
        mMap.addMarker(
                MarkerOptions().position(cpLatLng).title("START").icon(
                        BitmapDescriptorFactory.fromResource(R.drawable.pinpointstart)))
        mMap.moveCamera(CameraUpdateFactory.zoomTo(20f))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(cpLatLng))
    }

    private fun historyMapDeleteOnClickAction() {
        if (activity != null) {
            repo!!.deleteActivity(activity!!)

            val intent = Intent(this, HistoryActivity::class.java)
            intent.putExtra(C.TOKEN, token)
            intent.putExtra(C.FIRST_NAME, firstName)
            intent.putExtra(C.LAST_NAME, lastName)
            intent.putExtra(C.EMAIL, email)
            startActivity(intent)
        }
    }

    private fun historyMapReturnOnClickAction() {
        val intent = Intent(this, HistoryActivity::class.java)
        intent.putExtra(C.TOKEN, token)
        intent.putExtra(C.FIRST_NAME, firstName)
        intent.putExtra(C.LAST_NAME, lastName)
        intent.putExtra(C.EMAIL, email)
        startActivity(intent)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        mMap.isMyLocationEnabled = true

        populateMap()
    }

    private fun populateMap() {
        val parser = GPXParser()
        var parsedGpx: Gpx? = null
        try {
            val input: InputStream = this.openFileInput(activity!!.getFilePath())

            parsedGpx = parser.parse(input)
        } catch (e: Exception) {
            e.printStackTrace()
            historyMapReturnOnClickAction()
        }

        if (parsedGpx == null) {
            Log.d("GPXParsing", "failed to parse gpx")
            historyMapReturnOnClickAction()
        } else {
            val trackPoints = parsedGpx.tracks[0].trackSegments[0].trackPoints
            markStartToTheMap(trackPoints[0])
            trackPoints.forEach {
                extendPolyline(it)
            }
            markFinishToTheMap(trackPoints[trackPoints.size - 1])

            val wayPoints = parsedGpx.wayPoints
            wayPoints.forEach {
                markCheckPoint(it)
            }
        }
    }
}
package com.example.runtracker

class C {

    companion object {
        val NOTIFICATION_CHANNEL = "default_channel"
        val NOTIFICATION_ACTION_WP = "com.example.runtracker.wp"
        val NOTIFICATION_ACTION_CP = "com.example.runtracker.cp"

        val LOCATION_UPDATE_ACTION = "com.example.runtracker.location_update"

        val GPX_STOP_SESSION = "com.example.runtracker.stop_session"
        val GPX_PUT_NEW_WAYPOINT = "com.example.runtracker.new_trackpoint"

        val DISTANCE = "com.example.runtracker.distance"
        val DURATION = "com.example.runtracker.duration"
        val PACE = "com.example.runtracker.pace"

        val FIRST_NAME = "com.example.runtracker.firstName"
        val LAST_NAME = "com.example.runtracker.lastName"
        val TOKEN = "com.example.runtracker.token"
        val EMAIL = "com.example.runtracker.email"
        val SESSIONID = "com.example.runtracker.sessionId"
        val SENDING_SESSIONID = "com.example.runtracker.sendingSessionId"

        val ONRESUME_WP_MARKER = "com.example.runtracker.OnResumeWPMarker"
        val ONRESUME_CP_MARKERS = "com.example.runtracker.OnResumeCPMarkers"
        val ONRESUME_GENERAL_MARKERS = "com.example.runtracker.OnResumeGeneralMarkers"

        val LOCATION_MARK_CP_ACTION = "com.example.runtracker.mark_cp"
        val LOCATION_MARK_WP_ACTION = "com.example.runtracker.mark_WP"

        val LOCATION_UPDATE_LATITUDE = "com.example.runtracker.location_update.latitude"
        val LOCATION_UPDATE_LONGITUDE = "com.example.runtracker.location_update.longitude"
        val LOCATION_UPDATE_ALTITUDE = "com.example.runtracker.location_update.altitude"
        val LOCATION_UPDATE_TOTAL_DISTANCE = "com.example.runtracker.location_update.totDist"
        val LOCATION_UPDATE_TOTAL_SPEED = "com.example.runtracker.location_update.toSpeed"
        val LOCATION_UPDATE_CP_DISTANCE_TOTAL = "com.example.runtracker.location_update.cpDistTot"
        val LOCATION_UPDATE_CP_DISTANCE_DIRECT = "com.example.runtracker.location_update.cpDistDir"
        val LOCATION_UPDATE_WP_DISTANCE_TOTAL = "com.example.runtracker.location_update.wpDistTot"
        val LOCATION_UPDATE_WP_DISTANCE_DIRECT = "com.example.runtracker.location_update.wpDistDir"

        val TIMER_WP_BASE = "com.example.runtracker.wpTimerBase"
        val TIMER_CP_BASE = "com.example.runtracker.cpTimerBase"

        val ISSYNCED = "com.example.runtracker.isSynced"
        val ASKIFSYNCED = "com.example.runtracker.askIfSynced"
        val FINISHGPX = "com.example.runtracker.finishgpx"

        val BE_PUT_NEW_GPSPOINT = "com.example.runtracker.be_put_new_gps_point"
        val BE_START_NEW_BE_SERVICE = "com.example.runtracker.be_start_new_be_service"
        val SERIALIZED_GPS_POINT = "com.example.runtracker.serialized_gps_point"

        val NOTIFICATION_ID = 4321
    }
}
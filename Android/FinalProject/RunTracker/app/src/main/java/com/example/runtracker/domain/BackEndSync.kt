package com.example.runtracker.domain

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "backendsync")
class BackEndSync(var gpsPoint: String, var isSynced: Boolean, var sessionId: String) {

    @PrimaryKey(autoGenerate = true)
    var id_backEndSync : Int = 0



}
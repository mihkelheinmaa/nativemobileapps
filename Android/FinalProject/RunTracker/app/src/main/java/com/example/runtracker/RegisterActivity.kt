package com.example.runtracker

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.runtracker.domain.Registrant
import kotlinx.serialization.json.Json
import org.json.JSONObject

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        findViewById<Button>(R.id.button_logInActivity_register).setOnClickListener { registerButtonOnClick() }
    }

    private fun registerButtonOnClick() {

        val registrant = Registrant(
            findViewById<EditText>(R.id.fieldLogInUserName).text.toString(),
            findViewById<EditText>(R.id.fieldLogInPassword).text.toString(),
            findViewById<EditText>(R.id.fieldLogInLastName).text.toString(),
            findViewById<EditText>(R.id.fieldLogInFirstName).text.toString()
        )

        val registrantJson = Json.encodeToString(Registrant.serializer(), registrant)
        val registrantJson2 = JSONObject(registrantJson)

        val url = "https://sportmap.akaver.com/api/v1.0/account/register"

        val handler = HttpSingletonHandler.getInstance(this)

        val httpRequest = object: JsonObjectRequest(Method.POST, url, registrantJson2,
            Response.Listener { response ->
                val token = response.getString("token")
                val firstName = response.getString("firstName")
                val lastName = response.getString("lastName")

                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("isLoggedIn", true)
                intent.putExtra(C.TOKEN, token)
                intent.putExtra(C.FIRST_NAME, firstName)
                intent.putExtra(C.LAST_NAME, lastName)
                intent.putExtra(C.EMAIL, registrant.email)

                startActivity(intent)
            },
            Response.ErrorListener {
                  //  response -> händli halba requesti, kui aega jääb.
                 })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                return headers
            }
        }

        handler.addToRequestQueue(httpRequest)
    }


}
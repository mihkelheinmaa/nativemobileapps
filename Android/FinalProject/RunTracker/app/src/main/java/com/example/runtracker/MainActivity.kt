package com.example.runtracker

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageButton
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private var isLoggedIn : Boolean = false
    private var token : String = ""
    private var firstName : String = ""
    private var lastName : String = ""
    private var email : String = ""

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        isLoggedIn = intent.getBooleanExtra("isLoggedIn", false)
        token = intent.getStringExtra(C.TOKEN).toString()
        firstName = intent.getStringExtra(C.FIRST_NAME).toString()
        lastName = intent.getStringExtra(C.LAST_NAME).toString()
        email = intent.getStringExtra(C.EMAIL).toString()

        val newButton = findViewById<Button>(R.id.buttonNewActivity)
        val history = findViewById<Button>(R.id.buttonHistory)
        val register = findViewById<Button>(R.id.buttonRegister)
        val logIn = findViewById<Button>(R.id.buttonLogIn)

        if (!isLoggedIn) {
            history.isEnabled = false
            newButton.isEnabled = false
        } else if (isLoggedIn){
            logIn.isEnabled = false
            register.isEnabled = false
        }

        findViewById<Button>(R.id.buttonNewActivity).setOnClickListener { newActivityButtonOnClick() }
        findViewById<Button>(R.id.buttonRegister).setOnClickListener { registerButtonOnClick() }
        findViewById<Button>(R.id.buttonLogIn).setOnClickListener { logInButtonOnClick() }
        findViewById<Button>(R.id.buttonHistory).setOnClickListener { historyButtonOnClick() }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopService(Intent(this, TrackerService::class.java))
    }

    private fun newActivityButtonOnClick() {
        Log.d("nimiMain", firstName)
        val intent = Intent(this, TrackingActivity::class.java)
        setIntentExtras(intent)
        startActivity(intent)
    }

    private fun historyButtonOnClick() {
        val intent = Intent(this, HistoryActivity::class.java)
        setIntentExtras(intent)
        startActivity(intent)
    }

    private fun setIntentExtras(intent: Intent): Intent {
        intent.putExtra(C.TOKEN, token)
        intent.putExtra(C.FIRST_NAME, firstName)
        intent.putExtra(C.LAST_NAME, lastName)
        intent.putExtra(C.EMAIL, email)

        return intent
    }

    private fun logInButtonOnClick() {
        val intent = Intent(this, LogInActivity::class.java)
        startActivity(intent)
    }


    private fun registerButtonOnClick() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }
}
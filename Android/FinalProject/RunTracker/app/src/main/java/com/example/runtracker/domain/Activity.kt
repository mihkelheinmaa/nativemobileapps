package com.example.runtracker.domain

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@Entity(tableName = "activity")
class Activity {

    @PrimaryKey(autoGenerate = true)
    private var id_activity : Int = 0

    @ForeignKey(
        entity = Person::class,
        parentColumns = ["id_person"],
        childColumns = ["id_fkPerson"],
        onDelete = CASCADE
    )

    private var id_fkPerson : Int = 0
    @RequiresApi(Build.VERSION_CODES.O)
    private var savedAt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"))
    private var distance : String = ""
    private var duration : String = ""
    private var pace : String = ""
    private var filePath : String = ""
    private var sessionId : String = ""

    @RequiresApi(Build.VERSION_CODES.O)
    constructor(id_fkPerson: Int, distance: String, duration: String, pace: String, filePath: String, sessionId: String) {
        this.id_fkPerson = id_fkPerson
        this.distance = distance
        this.duration = duration
        this.pace = pace
        this.filePath = filePath
        this.sessionId = sessionId
    }

    fun getId_activity(): Int{
        return id_activity
    }
    fun setId_activity(id: Int) {
        id_activity = id
    }


    fun getId_fkPerson(): Int{
        return id_fkPerson
    }
    fun setId_fkPerson(id: Int) {
        id_fkPerson = id
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun getSavedAt(): String{
        return savedAt
    }
    @RequiresApi(Build.VERSION_CODES.O)
    fun setSavedAt(time: String) {
        savedAt = time
    }


    fun getDistance(): String{
        return distance
    }
    fun setDistance(dist: String) {
        distance = dist
    }


    fun getDuration(): String{
        return duration
    }
    fun setDuration(dur: String) {
        duration = dur
    }


    fun getPace(): String{
        return pace
    }
    fun setPace(pac: String) {
        pace = pac
    }


    fun getFilePath(): String{
        return filePath
    }
    fun setFilePath(path: String) {
        filePath = path
    }

    fun getSessionId(): String{
        return sessionId
    }
    fun setSessionId(id: String) {
        sessionId = id
    }
}
package com.example.runtracker.domain

import java.io.Serializable

@kotlinx.serialization.Serializable
class GpxPoint: Serializable {

    var recordedAt: String
    var isFirst: Boolean = false
    var isSynced: Boolean = false
    var latitude : Double
    var longitude : Double
    var accuracy: Float
    var altitude: Double
    var verticalAccuracy : Float
    var gpsSessionId: String? = null
    var gpsLocationTypeId : String

    constructor(recordedAt: String, latitude: Double, longitude: Double, accuracy: Float, altitude: Double, verticalAccuracy: Float, gpsLocationTypeId: String) {

        this.recordedAt = recordedAt
        this.latitude = latitude
        this.longitude = longitude
        this.accuracy = accuracy
        this.altitude = altitude
        this.verticalAccuracy = verticalAccuracy
        this.gpsLocationTypeId = gpsLocationTypeId
    }
}
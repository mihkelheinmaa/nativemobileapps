package com.example.runtracker

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import android.os.Build
import android.os.IBinder
import android.os.StrictMode
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.runtracker.dal.Repository
import com.example.runtracker.domain.BackEndSync
import com.example.runtracker.domain.GpxPoint
import kotlinx.serialization.json.Json
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.json.JSONObject
import java.lang.NullPointerException
import java.net.InetAddress
import java.net.UnknownHostException
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set


class DatabaseService : Service() {

    private val broadcastReceiver = InnerBroadcastReceiver()
    private val broadcastReceiverIntentFilter: IntentFilter = IntentFilter()
    private var token = ""
    private var sessionId = ""

    override fun onCreate() {
        Log.d("backEndService", "onCreate")
        super.onCreate()

        if (Build.VERSION.SDK_INT > 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }

        broadcastReceiverIntentFilter.addAction(C.GPX_STOP_SESSION)
        broadcastReceiverIntentFilter.addAction(C.BE_PUT_NEW_GPSPOINT)

        LocalBroadcastManager.getInstance(this).registerReceiver(
                broadcastReceiver,
                broadcastReceiverIntentFilter
        )
    }

    override fun onDestroy() {
        Log.d("backEndService", "onDestroy")
        super.onDestroy()

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("backEndService", "onStartCommand")

        token = intent!!.getStringExtra(C.TOKEN).toString()
        startNewSession()

        return START_STICKY
    }

    private fun startNewSession() {
        Log.d("backEndService", "onStartNewSession")
        val url = "https://sportmap.akaver.com/api/v1.0/GpsSessions"

        val handler = HttpSingletonHandler.getInstance(applicationContext)

        val payloadJson = JSONObject()
        payloadJson.put("name", "RunTracker test ${getTime()}")
        payloadJson.put("description", "Testing")
//        payloadJson.put("recordedAt", getTime())
        payloadJson.put("paceMin", 6 * 60)
        payloadJson.put("paceMax", 18 * 60)

        val httpRequest = object : JsonObjectRequest(
                Method.POST, url, payloadJson,
                Response.Listener<JSONObject>
                { response ->
                    sessionId = response.getString("id")
                    val mIntent = Intent(this, PushGPSPointService::class.java)
                    mIntent.putExtra(C.SESSIONID, sessionId)
                    mIntent.putExtra(C.TOKEN, token)
                    startService(mIntent)

                    val nIntent = Intent(C.SENDING_SESSIONID)
                    nIntent.putExtra(C.SESSIONID, sessionId)
                    LocalBroadcastManager.getInstance(this).sendBroadcast(nIntent)

                    Log.d("backEndService", "Received sessionId: $sessionId")
                },
                Response.ErrorListener {
                    try {
                        val idHash = getTime().hashCode()
                        sessionId = "FAILEDTOGETSESSIONID" + idHash
                        val mIntent = Intent(this, PushGPSPointService::class.java)
                        mIntent.putExtra(C.SESSIONID, sessionId)
                        mIntent.putExtra(C.TOKEN, token)
                        startService(mIntent)

                        val nIntent = Intent(C.SENDING_SESSIONID)
                        nIntent.putExtra(C.SESSIONID, sessionId)
                        LocalBroadcastManager.getInstance(this).sendBroadcast(nIntent)
                        Log.d("DatabaseService","FAIL on getting sessionId: ${it.toString()}")
                        Log.d("DatabaseService", "FAIL on getting sessionId: ERROR")
                        Log.d("DatabaseService", "FAIL on getting sessionId: ${it.networkResponse.statusCode}")
                        Log.d("DatabaseService", "FAIL on getting sessionId: ${it.networkResponse.data.decodeToString()}")

                    } catch (e: NullPointerException) {
                        // Must be safe
                    }

                }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["accept"] = "application/json"
                headers["Authorization"] = "Bearer $token"
                headers["Content-Type"] = "application/json"
                return headers
            }
        }
        handler.addToRequestQueue(httpRequest)
    }



    private fun getTime(): String? {
        val time = DateTime.now()
        val format: org.joda.time.format.DateTimeFormatter = ISODateTimeFormat.dateTime()
        return format.print(time)
    }


    private fun saveToTable(point: GpxPoint) {
        val repo = Repository(application)

        val pointForTable = BackEndSync(Json.encodeToString(GpxPoint.serializer(), point), false, sessionId)

        repo.insertBackEndSync(pointForTable)
        Log.d("PushGPSPointService", "onSaveToTable ||| POINT SAVED TO DB SessionID: $sessionId")

    }


    override fun onBind(intent: Intent?): IBinder? {
        TODO("Not yet implemented")
    }


    private inner class InnerBroadcastReceiver : BroadcastReceiver() {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent!!.action) {
                C.GPX_STOP_SESSION -> {
                    onDestroy()
                }
                C.BE_PUT_NEW_GPSPOINT -> {
//                    gpsPoints.add(intent.getSerializableExtra(C.SERIALIZED_GPS_POINT) as GpxPoint)
                    saveToTable(intent.getSerializableExtra(C.SERIALIZED_GPS_POINT) as GpxPoint)
                }
            }
        }
    }
}


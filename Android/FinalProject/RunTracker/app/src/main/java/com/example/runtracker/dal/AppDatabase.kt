package com.example.runtracker.dal

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.runtracker.domain.Activity
import com.example.runtracker.domain.BackEndSync
import com.example.runtracker.domain.Person
import com.example.runtracker.domain.PersonDao

@Database(
    entities = [Person::class, Activity::class, BackEndSync::class],
    version = 4
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun PersonDao(): PersonDao

    companion object {
        @Volatile private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
            AppDatabase::class.java, "runTracker.db")
            .fallbackToDestructiveMigration()
            .build()
    }
}
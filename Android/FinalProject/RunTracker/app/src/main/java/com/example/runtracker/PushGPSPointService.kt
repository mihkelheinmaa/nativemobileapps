package com.example.runtracker

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.runtracker.dal.Repository
import com.example.runtracker.domain.GpxPoint
import kotlinx.serialization.json.Json
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.json.JSONObject
import java.lang.NullPointerException
import java.net.InetAddress
import java.net.UnknownHostException
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap
import kotlin.collections.set


class PushGPSPointService : Service() {

    private val worker: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()

    private val broadcastReceiver = InnerBroadcastReceiver()
    private val broadcastReceiverIntentFilter: IntentFilter = IntentFilter()

    private var token = ""
    private var sessionId = ""
    private val gpsLocationURL = "https://sportmap.akaver.com/api/v1.0/GpsLocations"
    private var reTryCounter = 0
//    private var gpsPoints: ArrayList<GpxPoint> = ArrayList()
    private var isRunning = true


    override fun onCreate() {
        Log.d("PushGPSPointService", "onCreate")
        super.onCreate()

        broadcastReceiverIntentFilter.addAction(C.GPX_STOP_SESSION)

        LocalBroadcastManager.getInstance(this).registerReceiver(
                broadcastReceiver,
                broadcastReceiverIntentFilter
        )
    }

    private fun getPointsFromDb() {
        reTryCounter = 0
        Log.d("PushGPSPointService", "onGetPointsFromDb: ${sessionId.substring(0, 4)} <---")
        if (sessionId.substring(0, 4) == "FAIL") {
            Log.d("PushGPSPointService", "ONSUBSTRINGTHREAD")
            handleFailedSessionId()
            Thread.sleep(7000)
        }
        val repo = Repository(application)

        Thread.sleep(5000)

        Log.d("PushGPSPointService", "HERE 1 . SessionId: ${sessionId}")
        var pointsToSendToBackEnd = repo.getBackEndSyncBySessionId(sessionId).distinctBy { p -> p.id_backEndSync }

        var lastSize = 0

        Log.d("PushGPSPointService", "HERE 2, size: ${pointsToSendToBackEnd.size}")
        while (reTryCounter < 5) {
            Log.d("PushGPSPointService", "SIZEEEE OF PULLED DATASET: ${pointsToSendToBackEnd.size}")

            val isConnected = hasInternet()

            if (isConnected) {
                if (pointsToSendToBackEnd.size > 0) {
                    reTryCounter = 0
                } else {
                    reTryCounter++
                }
                pointsToSendToBackEnd.forEach {
                    pushPointOut(Json.decodeFromString(GpxPoint.serializer(), it.gpsPoint))
                    repo.updateBackEndSync(it.id_backEndSync, true)
                }
            }
            if (!isConnected) {
                var tableSize = repo.getBackEndPointsTableSize(sessionId)
                Log.d("PushGPSPointService", "TABLE SIZE: ${tableSize}")
                if (lastSize == tableSize) {
                    reTryCounter++
                } else {
                    reTryCounter = 0
                }
                if (reTryCounter > 10) {
                    onDestroy()
                }
                lastSize = tableSize
            }
            Log.d("PushGPSPointService", "ReTryCounter: ${reTryCounter}")
            Thread.sleep(7000)
            pointsToSendToBackEnd = repo.getBackEndSyncBySessionId(sessionId).distinctBy { p -> p.id_backEndSync }
        }
        Log.d("PushGPSPointService", "HERE 3")
        onDestroy()
    }

    private fun handleFailedSessionId() {
        Log.d("PushGPSPointService", "onHandleFailedSessionId")
        val repo = Repository(application)
        val newSessionId = getNewSessionId()

        updateBackEndSessionIds(sessionId, newSessionId)
        Thread.sleep(2000)
        Log.d("PushGPSPointService", "NEW SESSION ID: ${newSessionId}")
        repo.updateActivityNewSessionId(sessionId, newSessionId)
        Thread.sleep(2000)
        sessionId = newSessionId
    }

    private fun updateBackEndSessionIds(sessionId: String, newSessionId: String) {
        val repo = Repository(application)
        repo.updateAllBackEndSyncSessionIdBySessionId(sessionId, newSessionId)
    }



    private fun getTime(): String? {
        val time = DateTime.now()
        val format: org.joda.time.format.DateTimeFormatter = ISODateTimeFormat.dateTime()
        return format.print(time)
    }

    private fun getNewSessionId(): String {
        Log.d("PushGPSPointService", "onGetNewSessionId")
        val url = "https://sportmap.akaver.com/api/v1.0/GpsSessions"

        val handler = HttpSingletonHandler.getInstance(applicationContext)

        val payloadJson = JSONObject()
        payloadJson.put("name", "RunTracker test ${getTime()}")
        payloadJson.put("description", "Testing")
//        payloadJson.put("recordedAt", getTime())
        payloadJson.put("paceMin", 6 * 60)
        payloadJson.put("paceMax", 18 * 60)

        val idHash = getTime().hashCode()
        var sessionId = "FAILEDTOGETSESSIONID" + idHash

        val httpRequest = object : JsonObjectRequest(
                Method.POST, url, payloadJson,
                Response.Listener<JSONObject>
                { response ->
                    sessionId = response.getString("id")
                    Log.d("PushGPSPointService", "onGetNewSessionId NEW ID: $sessionId")
                },
                Response.ErrorListener {
                    try {
                        Log.d("PushGPSPointService","FAIL on getting sessionId: ${it.toString()}")
                        Log.d("PushGPSPointService", "FAIL on getting sessionId: ERROR")
                        Log.d("PushGPSPointService", "FAIL on getting sessionId: ${it.networkResponse.statusCode}")
                        Log.d("PushGPSPointService", "FAIL on getting sessionId: ${it.networkResponse.data.decodeToString()}")

                    } catch (e: NullPointerException) {
                        // Must be safe
                    }

                }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["accept"] = "application/json"
                headers["Authorization"] = "Bearer $token"
                headers["Content-Type"] = "application/json"
                return headers
            }
        }
        handler.addToRequestQueue(httpRequest)
        Thread.sleep(3000)
        return sessionId
    }

    private fun looper() {
        Log.d("PushGPSPointService", "onLooper")

        val task = Runnable {
            Log.d("PushGPSPointService", "LOOPER CALLED")
            getPointsFromDb()
        }
        worker.schedule(task, 5, TimeUnit.SECONDS)
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("PushGPSPointService", "onStartCommand")

        sessionId = intent!!.getStringExtra(C.SESSIONID).toString()
        token = intent!!.getStringExtra(C.TOKEN).toString()
        Log.d("PushGPSPointService", "RECEIVED TOKEN: $token")
        Log.d("PushGPSPointService", "RECEIVED SESSIONID: $sessionId")

        looper()

        return START_STICKY
    }

    override fun onDestroy() {
        Log.d("PushGPSPointService", "onDestroy")
        super.onDestroy()

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }


    private fun hasInternet(): Boolean {
        try {
            val address: InetAddress = InetAddress.getByName("www.google.com")
            return !address.equals("")
        } catch (e: UnknownHostException) {
            // Log error
        }
        return false
    }

    private fun pushPointOut(point: GpxPoint) {
        Log.d("PushGPSPointService", "onPushPointOut RECORDEDAT ||| ${point.recordedAt}")
        Log.d("PushGPSPointService", "onPushPointOut TOKEN ||| ${token}")
        Log.d("PushGPSPointService", "onPushPointOut SESSIONID ||| ${sessionId}")
        val handler = HttpSingletonHandler.getInstance(applicationContext)

        val payloadJson = JSONObject()
//        payloadJson.put("recordedAt", point.recordedAt)
        payloadJson.put("latitude", point.latitude)
        payloadJson.put("longitude", point.longitude)
        payloadJson.put("accuracy", point.accuracy)
        payloadJson.put("altitude", point.altitude)
        payloadJson.put("verticalAccuracy", point.verticalAccuracy)
        payloadJson.put("gpsSessionId", sessionId)
        payloadJson.put("gpsLocationTypeId", point.gpsLocationTypeId)


        val httpRequest = object : JsonObjectRequest(
                Method.POST, gpsLocationURL, payloadJson,
                Response.Listener
                { response ->
                    Log.d("PushGPSPointService", "Location sending response: $response")
                    point.isSynced = true
                },
                Response.ErrorListener {
                    try {
                        Log.d("backEndService", it.toString())
                        Log.d("backEndService", "ERROR")
                        Log.d("backEndService", "FAIL: ${it.networkResponse.statusCode}")
                        Log.d("backEndService", "FAIL: ${it.networkResponse.data.decodeToString()}")

                    } catch (e: NullPointerException) {
                        // Must be safe
                    }

                }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["accept"] = "application/json"
                headers["Authorization"] = "Bearer $token"
                headers["Content-Type"] = "application/json"
                return headers
            }
        }
        handler.addToRequestQueue(httpRequest)


    }


    override fun onBind(intent: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

    private inner class InnerBroadcastReceiver : BroadcastReceiver() {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent!!.action) {
                C.GPX_STOP_SESSION -> {
                    isRunning = false
                }
            }
        }
    }
}
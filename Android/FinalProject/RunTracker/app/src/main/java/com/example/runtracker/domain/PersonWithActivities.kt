package com.example.runtracker.domain

import androidx.room.Embedded
import androidx.room.Relation

class PersonWithActivities {

    @Embedded
    var person: Person? = null

    @Relation(parentColumn = "id_person", entityColumn = "id_fkPerson")
    var activities: List<Activity>? = null

    fun CourseWithStudents(person: Person, activities: List<Activity>) {
        this.person = person
        this.activities = activities
    }
}
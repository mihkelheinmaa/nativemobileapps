package com.example.runtracker

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.runtracker.domain.LogInner
import com.example.runtracker.domain.Person
import kotlinx.serialization.json.Json
import org.json.JSONObject
import com.example.runtracker.dal.Repository

class LogInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        findViewById<Button>(R.id.button_logInActivity_register).setOnClickListener { registerButtonOnClick() }
        findViewById<Button>(R.id.button_logInActivity_logIn).setOnClickListener { logInButtonOnClick() }
    }

    private fun registerButtonOnClick() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    private fun logInButtonOnClick() {
        val logInner = LogInner(
            findViewById<EditText>(R.id.fieldLogInUserName).text.toString(),
            findViewById<EditText>(R.id.fieldLogInPassword).text.toString(),
        )

        val registrantJson = Json.encodeToString(LogInner.serializer(), logInner)
        val registrantJson2 = JSONObject(registrantJson)

        val url = "https://sportmap.akaver.com/api/v1.0/account/login"

        val handler = HttpSingletonHandler.getInstance(this)

        val httpRequest = object: JsonObjectRequest(
            Method.POST, url, registrantJson2,
            Response.Listener { response ->
                val token = response.getString("token")
                val firstName = response.getString("firstName")
                val lastName = response.getString("lastName")
                Log.d("responsetoken", response.toString())

                checkIfInDatabase(logInner.email, firstName, lastName, token)

                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("isLoggedIn", true)
                intent.putExtra(C.TOKEN, token)
                intent.putExtra(C.FIRST_NAME, firstName)
                intent.putExtra(C.LAST_NAME, lastName)
                intent.putExtra(C.EMAIL, logInner.email)

                startActivity(intent)
            },
            Response.ErrorListener { error ->
                Log.d("responsetoken", error.toString())
                findViewById<TextView>(R.id.errorMessageField).text = getString(R.string.logInFailed)
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                return headers
            }
        }

        handler.addToRequestQueue(httpRequest)
    }

    private fun checkIfInDatabase(email: String, firstName: String, lastName: String, token: String) {

        val repo = Repository(application)

        val persons = repo.getPersonInFocus(email, firstName)

        if (persons.isEmpty()) {
            repo.insertPerson(Person(email, firstName, lastName, token))
        }
    }
}
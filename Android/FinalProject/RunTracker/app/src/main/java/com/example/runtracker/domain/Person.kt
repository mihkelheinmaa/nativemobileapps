package com.example.runtracker.domain

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "person")
class Person {

    @PrimaryKey(autoGenerate = true)
    private var id_person : Int = 0
    private var firstname : String = ""
    private var lastname : String = ""
    private var token : String = ""
    private var email : String = ""

    constructor(email: String, firstname: String, lastname: String, token: String) {
        this.firstname = firstname
        this.lastname = lastname
        this.token = token
        this.email = email
    }

    fun getId_person(): Int{
        return id_person
    }
    fun setId_person(id: Int) {
        id_person = id
    }


    fun getFirstname(): String{
        return firstname
    }
    fun setFirstname(name: String) {
        firstname = name
    }


    fun getLastname(): String{
        return lastname
    }
    fun setLastname(name: String) {
        lastname = name
    }


    fun getToken(): String{
        return token
    }
    fun setToken(token: String) {
        this.token = token
    }


    fun getEmail(): String{
        return email
    }
    fun setEmail(email: String) {
        this.email = email
    }
}
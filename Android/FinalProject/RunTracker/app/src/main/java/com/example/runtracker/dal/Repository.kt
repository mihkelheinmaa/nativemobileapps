package com.example.runtracker.dal

import android.app.Application
import com.example.runtracker.domain.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future


class Repository {

    lateinit var personDao: PersonDao

    constructor(application: Application) {
        var db = AppDatabase(application)
        personDao = db.PersonDao()
    }

    fun insertPerson(person: Person) {
        val callable: Callable<Unit> = Callable { personDao.insertPerson(person) }

        var future: Future<Unit> = Executors.newSingleThreadExecutor().submit(callable)
    }

    fun insertBackEndSyncs(backEndSyncs: List<BackEndSync>) {
        val callable: Callable<Unit> = Callable { personDao.insertBackEndSyncs(backEndSyncs) }

        var future: Future<Unit> = Executors.newSingleThreadExecutor().submit(callable)
    }

    fun deleteActivity(activity: Activity) {
        val callable: Callable<Unit> = Callable { personDao.deleteActivity(activity) }

        var future: Future<Unit> = Executors.newSingleThreadExecutor().submit(callable)
    }

    fun insertActivity(activity: Activity) {
        val callable: Callable<Unit> = Callable { personDao.insertActivity(activity) }

        var future: Future<Unit> = Executors.newSingleThreadExecutor().submit(callable)
    }

    fun insertBackEndSync(backEndSync: BackEndSync) {
        val callable: Callable<Unit> = Callable { personDao.insertBackEndSync(backEndSync)}

        var future: Future<Unit> = Executors.newSingleThreadExecutor().submit(callable)
    }

    fun findActivitiesByPersonId(personID: Int): List<Activity> {
        val callable: Callable<List<Activity>> = Callable { personDao.findActivitiesByPersonId(personID) }

        val future = Executors.newSingleThreadExecutor().submit(callable)

        return future.get()
    }

    fun getPersonInFocus(email: String, firstname: String): List<Person> {
        val callable: Callable<List<Person>> = Callable { personDao.getPersonInFocus(email, firstname) }

        val future = Executors.newSingleThreadExecutor().submit(callable)

        return future.get()
    }

    fun getActivityById(id: Int): Activity {
        val callable: Callable<List<Activity>> = Callable { personDao.getActivityById(id) }

        val future = Executors.newSingleThreadExecutor().submit(callable)

        return future.get()[0]
    }

    fun getBackEndSyncBySessionId(id: String): List<BackEndSync> {
        val callable: Callable<List<BackEndSync>> = Callable { personDao.getBackEndSyncBySessionId(id) }

        val future = Executors.newSingleThreadExecutor().submit(callable)

        return future.get()
    }

    fun getBackEndPointsTableSize(id: String): Int {
        val callable: Callable<Int> = Callable { personDao.getBackEndPointsTableSize(id) }

        val future = Executors.newSingleThreadExecutor().submit(callable)

        return future.get()
    }

    fun getBackEndSyncBySessionIdAll(id: String): List<BackEndSync> {
        val callable: Callable<List<BackEndSync>> = Callable { personDao.getBackEndSyncBySessionId(id) }

        val future = Executors.newSingleThreadExecutor().submit(callable)

        return future.get()
    }

    fun updateBackEndSync(id: Int, newStatus: Boolean) {
        val callable: Callable<Unit> = Callable { personDao.updateBackEndSync(id, newStatus) }

        var future: Future<Unit> = Executors.newSingleThreadExecutor().submit(callable)
    }

    fun updateAllBackEndSyncSessionIdBySessionId(sessionId: String, newSessionId: String) {
        val callable: Callable<Unit> = Callable { personDao.updateAllBackEndSyncSessionIdBySessionId(sessionId, newSessionId) }

        var future: Future<Unit> = Executors.newSingleThreadExecutor().submit(callable)
    }

    fun updateActivityNewSessionId(id: String, newSessId: String) {
        val callable: Callable<Unit> = Callable { personDao.updateActivityNewSessionId(id, newSessId) }

        var future: Future<Unit> = Executors.newSingleThreadExecutor().submit(callable)
    }



    fun getAll(): List<Person> {
        val callable: Callable<List<Person>> = Callable { personDao.getAll() }

        val future = Executors.newSingleThreadExecutor().submit(callable)

        return future.get()
    }

}
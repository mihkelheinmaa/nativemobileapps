package com.example.runtracker

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.runtracker.dal.Repository
import com.example.runtracker.domain.Activity
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import java.io.*


class GpxService: Service() {

    private var CPCounter = 1

    private val broadcastReceiver = InnerBroadcastReceiver()
    private val broadcastReceiverIntentFilter: IntentFilter = IntentFilter()

    private var beginningString = StringBuilder()
    private var trkStringBuilder = StringBuilder()
    private var wptStringBuilder = StringBuilder()

    private var personId = 0
    private var distance = ""
    private var duration = ""
    private var pace = ""

    private var firstName = ""
    private var lastName = ""
    private var email = ""
    private var sessionId = ""

    override fun onCreate() {
        Log.d("gpx", "onCreate")
        super.onCreate()

        broadcastReceiverIntentFilter.addAction(C.GPX_STOP_SESSION)
        broadcastReceiverIntentFilter.addAction(C.GPX_PUT_NEW_WAYPOINT)
        broadcastReceiverIntentFilter.addAction(C.LOCATION_UPDATE_ACTION)
        broadcastReceiverIntentFilter.addAction(C.SENDING_SESSIONID)

        LocalBroadcastManager.getInstance(this).registerReceiver(
            broadcastReceiver,
            broadcastReceiverIntentFilter
        )
    }


    override fun onDestroy() {
        Log.d("gpx", "onDestroy")
        super.onDestroy()

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        firstName = intent!!.getStringExtra(C.FIRST_NAME).toString()
        lastName = intent.getStringExtra(C.LAST_NAME).toString()
        email = intent.getStringExtra(C.EMAIL).toString()
        sessionId = intent.getStringExtra(C.SESSIONID).toString()

        beginningString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
        beginningString.append("<gpx version=\"1.1\" creator=\"RunTracker, a school project\" ")
        beginningString.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ")
        beginningString.append("xmlns=\"http://www.topografix.com/GPX/1/1\" ")
        beginningString.append("xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 ")
        beginningString.append("http://www.topografix.com/GPX/1/1/gpx.xsd\">\n")
        beginningString.append("<metadata>\n<author>\n<name>").append(intent.getStringExtra("$firstName $lastName")).append(
            "</name>\n</author>\n<time>"
        ).append(getTime()).append("</time>\n</metadata>\n")

        trkStringBuilder.append("<trk>\n<trkseg>\n")
        trkStringBuilder.append(
            "<trkpt lat=\"${intent.getDoubleExtra("lat", 0.0)}\" lon=\"${
                intent.getDoubleExtra(
                    "lng",
                    0.0
                )
            }\">\n"
        )
        trkStringBuilder.append("<time>${getTime()}</time>\n</trkpt>\n")

        return START_STICKY
    }


    fun markNewTrackPoint(lat: Double, lng: Double, alt: Double) {
        trkStringBuilder.append("<trkpt lat=\"${lat}\" lon=\"${lng}\">\n")
        trkStringBuilder.append("<ele>${alt}</ele>\n")
        trkStringBuilder.append("<time>${getTime()}</time>\n</trkpt>\n")
    }


    fun markNewWayPoint(lat: Double, lng: Double, alt: Double) {
        Log.d("gpx", "markingNewWP")
        wptStringBuilder.append("<wpt lat=\"${lat}\" lon=\"${lng}\">\n")
        wptStringBuilder.append("<ele>${alt}</ele>\n")
        wptStringBuilder.append("<name>Checkpoint Nr${CPCounter}</name>\n")
        wptStringBuilder.append("<time>${getTime()}</time>\n</wpt>\n")

        CPCounter++
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun finishAndSave(lat: Double, lng: Double, alt: Double) {
        Log.d("gpx", "finishingUp")
        trkStringBuilder.append("</trkseg>\n</trk>\n</gpx>")

        var fileContent = StringBuilder()
                .append(beginningString)
                .append(wptStringBuilder.toString())
                .append(trkStringBuilder.toString())

        val filename = "${getTime()}_activity.gpx"

        val outputStream: FileOutputStream

        try {
            outputStream = openFileOutput(filename, MODE_PRIVATE)
            outputStream.write(fileContent.toString().toByteArray())
            outputStream.close()
        } catch (e: Exception) {
            e.stackTrace
        }

        saveToDatabase(filename)
        onDestroy()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun saveToDatabase(path: String) {
        val repo = Repository(application)

        var persons = repo.getPersonInFocus(email, firstName)

        personId = persons[0].getId_person()

        Log.d("gpx", "onSaveToDatabase. SessionId: $sessionId")
        val session = Activity(personId, distance, duration, pace, path, sessionId)
        repo.insertActivity(session)

    }


    fun getTime(): String? {
        val time = DateTime.now()
        val format: org.joda.time.format.DateTimeFormatter = ISODateTimeFormat.dateTime()
        return format.print(time)

//        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT)
//        } else {
//            SimpleDateFormat.DEFAULT.toString()
//        }
    }





    override fun onBind(intent: Intent?): IBinder? {
        TODO("Not yet implemented")
    }


    private inner class InnerBroadcastReceiver: BroadcastReceiver() {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onReceive(context: Context?, intent: Intent?) {

            Log.d(TrackerService.TAG, intent!!.action.toString())
            when(intent!!.action){
                C.LOCATION_UPDATE_ACTION -> {
                    markNewTrackPoint(
                        intent.getDoubleExtra(C.LOCATION_UPDATE_LATITUDE, 0.0),
                        intent.getDoubleExtra(C.LOCATION_UPDATE_LONGITUDE, 0.0),
                        intent.getDoubleExtra(C.LOCATION_UPDATE_ALTITUDE, 0.0)
                    )
                }
                C.GPX_PUT_NEW_WAYPOINT -> {
                    markNewWayPoint(
                        intent.getDoubleExtra(C.LOCATION_UPDATE_LATITUDE, 0.0),
                        intent.getDoubleExtra(C.LOCATION_UPDATE_LONGITUDE, 0.0),
                        intent.getDoubleExtra(C.LOCATION_UPDATE_ALTITUDE, 0.0)
                    )

                }
                C.GPX_STOP_SESSION -> {
                    distance = intent.getStringExtra(C.DISTANCE).toString()
                    duration = intent.getStringExtra(C.DURATION).toString()
                    pace = intent.getStringExtra(C.PACE).toString()
                    finishAndSave(
                        intent.getDoubleExtra(C.LOCATION_UPDATE_LATITUDE, 0.0),
                        intent.getDoubleExtra(C.LOCATION_UPDATE_LONGITUDE, 0.0),
                        intent.getDoubleExtra(C.LOCATION_UPDATE_ALTITUDE, 0.0)
                    )
                }

                C.SENDING_SESSIONID -> {
                    sessionId = intent.getStringExtra(C.SESSIONID).toString()
                    Log.d("GpxService", "SESSIONID RECEIVED: $sessionId")
                }
            }
        }

    }
}
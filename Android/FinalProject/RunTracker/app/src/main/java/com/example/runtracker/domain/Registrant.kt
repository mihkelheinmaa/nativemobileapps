package com.example.runtracker.domain

import kotlinx.serialization.Serializable

@Serializable
class Registrant {
    var email: String = ""
    var password: String = ""
    var lastname: String = ""
    var firstname: String = ""

    constructor(email: String, password: String, lastname: String, firstname: String) {
        this.email = email
        this.password = password
        this.lastname = lastname
        this.firstname = firstname
    }
}
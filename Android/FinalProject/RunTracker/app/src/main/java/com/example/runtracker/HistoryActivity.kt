package com.example.runtracker

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.runtracker.dal.Repository
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.json.JSONObject
import java.lang.NullPointerException


class HistoryActivity : AppCompatActivity() {

    private var token : String = ""
    private var firstName : String = ""
    private var lastName : String = ""
    private var email : String = ""

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        token = intent.getStringExtra(C.TOKEN).toString()
        firstName = intent.getStringExtra(C.FIRST_NAME).toString()
        lastName = intent.getStringExtra(C.LAST_NAME).toString()
        email = intent.getStringExtra(C.EMAIL).toString()

        val repo = Repository(application)
        val personInFocus = repo.getPersonInFocus(email, firstName)
        val sessions = repo.findActivitiesByPersonId(personInFocus[0].getId_person())

        val view = findViewById<LinearLayout>(R.id.history_linear)

        val inflater: LayoutInflater = this.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater

        sessions.forEach {
            var sessionId = it.getSessionId()
            Log.d("HistoryActivity", "SESSION ID 1: ${sessionId}")
            val row: View = inflater.inflate(R.layout.row_view_history, findViewById(R.id.row_view))
            row.id = it.getId_activity()
            val button = row.findViewById<ImageButton>(R.id.row_button)
            button.setOnClickListener { activityOnClickAction(row.id) }
            row.findViewById<TextView>(R.id.history_date).text = it.getSavedAt().substring(0, 16)
            row.findViewById<TextView>(R.id.history_distance_field).text = it.getDistance()
            row.findViewById<TextView>(R.id.history_duration_field).text = it.getDuration()
            if (repo.getBackEndPointsTableSize(sessionId) > 0) {
                row.findViewById<Button>(R.id.pressToSyncButton).setOnClickListener {
                    val mIntent = Intent(this, PushGPSPointService::class.java)
                    mIntent.putExtra(C.SESSIONID, sessionId)
                    mIntent.putExtra(C.TOKEN, token)
                    startService(mIntent)
                    Log.d("HistoryActivity", "SESSION ID 2: ${sessionId}")
                    it.visibility = View.GONE
                }
            } else {
                row.findViewById<Button>(R.id.pressToSyncButton).visibility = View.GONE
            }

            view.addView(row)
        }

        val header = findViewById<TextView>(R.id.history_header)
        header.text = "${firstName}, here're your activities"

        findViewById<ImageButton>(R.id.historyMainReturnButton).setOnClickListener { returnButtonOnClickAction() }

    }


    private fun returnButtonOnClickAction() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("token", token)
        intent.putExtra(C.FIRST_NAME, firstName)
        intent.putExtra(C.LAST_NAME, lastName)
        intent.putExtra(C.EMAIL, email)
        intent.putExtra(C.TOKEN, token)
        intent.putExtra("isLoggedIn", true)
        startActivity(intent)
    }

    private fun activityOnClickAction(id: Int) {
        Log.d("historyMap", "activity clicked")
        val intent = Intent(this, PastSessionActivity::class.java)
        intent.putExtra("activityId", id)
        intent.putExtra(C.FIRST_NAME, firstName)
        intent.putExtra(C.EMAIL, email)
        intent.putExtra(C.LAST_NAME, lastName)
        startActivity(intent)
    }
}
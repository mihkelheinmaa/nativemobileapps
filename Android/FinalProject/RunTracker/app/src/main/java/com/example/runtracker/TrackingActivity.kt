package com.example.runtracker

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*


@Suppress("UNCHECKED_CAST")
class TrackingActivity : AppCompatActivity(), SensorEventListener, OnMapReadyCallback, LocationListener, PopupMenu.OnMenuItemClickListener {
    companion object {
        private val TAG = this::class.java.declaringClass!!.simpleName
    }

    private var token: String = ""
    private var firstName: String = ""
    private var lastName: String = ""
    private var email: String = ""

    private var lat: Double = 0.0
    private var lon: Double = 0.0

    private var isTracking: Boolean = false
    private var northIsUp: Boolean = true

    private lateinit var sensorManager: SensorManager
    private lateinit var compassImage: ImageView
    private lateinit var accelerometer: Sensor
    private lateinit var magnetometer: Sensor

    private lateinit var mMap: GoogleMap
    private lateinit var locationManager: LocationManager
    private var location: Location? = null
    private var provider : String? = ""
    private var polylineOptions = PolylineOptions().width(10f).color(Color.MAGENTA)
    private var polyline: Polyline? = null

    private var currentDegree = 0.0f
    private var lastAccelerometer = FloatArray(3)
    private var lastMagnetometer = FloatArray(3)
    private var lastAccelerometerSet = false
    private var lastMagnetometerSet = false

    private var timerTotal: Chronometer? = null
    private var timerCP: Chronometer? = null
    private var timerWP: Chronometer? = null

    private val broadcastReceiver = InnerBroadcastReceiver()
    private val broadcastReceiverIntentFilter: IntentFilter = IntentFilter()

    private var wpMarker: Marker? = null
    private var startMarkerCoordinates: ArrayList<Double> = ArrayList(2)
    private var finishMarkerCoordinates: ArrayList<Double> = ArrayList(2)
    private var wpMarkerCoordinates: ArrayList<Double> = ArrayList(2)
    private var cpMarkers: ArrayList<ArrayList<Double>> = ArrayList()
    private var allLocations: ArrayList<ArrayList<Double>> = ArrayList()

    private var savedInstanceState: Bundle? = null

    private var spinner: ProgressBar? = null

    private var fromResume = false
    private var isFirstBoot = true


    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracking)

        if (savedInstanceState != null) {
            isFirstBoot = savedInstanceState.getBoolean("isFirstBoot")
            this.savedInstanceState = savedInstanceState
        }

        spinner = findViewById(R.id.progressBar)
        spinner!!.visibility = View.GONE

        token = intent.getStringExtra(C.TOKEN).toString()
        firstName = intent.getStringExtra(C.FIRST_NAME).toString()
        lastName = intent.getStringExtra(C.LAST_NAME).toString()
        email = intent.getStringExtra(C.EMAIL).toString()

        createNotificationChannel()

        broadcastReceiverIntentFilter.addAction(C.LOCATION_UPDATE_ACTION)
        broadcastReceiverIntentFilter.addAction(C.LOCATION_MARK_CP_ACTION)
        broadcastReceiverIntentFilter.addAction(C.LOCATION_MARK_WP_ACTION)

        if (isTracking) {
            findViewById<Button>(R.id.button_start).text = getString(R.string.button_stop)
        }

        compassImage = findViewById(R.id.imageCompass)
        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.mappy) as SupportMapFragment
        mapFragment.getMapAsync(this)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val criteria = Criteria()
        provider = locationManager.getBestProvider(criteria, true)
        if (provider != null) {
            if (
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return
            }
            location = locationManager.getLastKnownLocation(provider!!)
        }

        findViewById<TextView>(R.id.startButtonText).text = getString(R.string.start_button)

        findViewById<ImageButton>(R.id.button_start).setOnClickListener { startButtonOnClick() }
        findViewById<ImageButton>(R.id.button_cp).setOnClickListener { checkpointButtonOnClick() }
        findViewById<ImageButton>(R.id.button_wp).setOnClickListener { wayPointButtonOnClick() }

        findViewById<ImageButton>(R.id.buttonReset).setOnClickListener { resetButtonOnClick() }
        findViewById<ImageButton>(R.id.buttonNorthUp).setOnClickListener { northUpButtonOnClick() }

        findViewById<ImageButton>(R.id.buttonCompass).setOnClickListener { onCompassBtnOnClick() }

        timerTotal = findViewById(R.id.total_time_field)
        timerCP = findViewById(R.id.cp_time_field)
        timerWP = findViewById(R.id.wp_time_field)

    }

    override fun onResume() {
        Log.d(TAG, "onResume")
        super.onResume()

        if (!isFirstBoot) {
            fromResume = true
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, broadcastReceiverIntentFilter)

        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME)
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_GAME)

        if (provider != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return
            }
            locationManager.requestLocationUpdates(provider!!, 500, 15f, this)
        }
        isFirstBoot = false
    }

    override fun onPause() {
        Log.d(TAG, "onPause")
        super.onPause()
        sensorManager.unregisterListener(this, accelerometer)
        sensorManager.unregisterListener(this, magnetometer)

        if (provider != null) {
            if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            locationManager.removeUpdates(this)
        }
    }

    override fun onStop() {
        Log.d(TAG, "onStop")
        super.onStop()

        if (isTracking) {
            if (polyline != null) {
                polyline!!.remove()
            }
        }


        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")
        super.onDestroy()
//        stopService(Intent(this, TrackerService::class.java))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        Log.d(TAG, "OnSaveInstance, firstboot: $isFirstBoot")

        outState.putBoolean("isTracking", isTracking)
        outState.putBoolean("isFirstBoot", isFirstBoot)
        outState.putBoolean("northIsUp", northIsUp)
        if (timerTotal != null) {
            outState.putLong("totalTimerBase", timerTotal!!.base)
            outState.putLong("totalTimerCP", timerCP!!.base)
            outState.putLong("totalTimerWP", timerWP!!.base)
        }

        outState.putString("distanceTotal", findViewById<TextView>(R.id.total_dist_field).text.toString())
        outState.putString("paceTotal", findViewById<TextView>(R.id.total_speed_field).text.toString())
        outState.putString("distanceWP", findViewById<TextView>(R.id.wp_distance_field).text.toString())
        outState.putString("distanceWPDirect", findViewById<TextView>(R.id.wp_dist_direct_field).text.toString())
        outState.putString("distanceCP", findViewById<TextView>(R.id.cp_distance_field).text.toString())
        outState.putString("distanceCPDirect", findViewById<TextView>(R.id.cp_dist_direct_field).text.toString())

        outState.putSerializable("startMarkerCoordinates", startMarkerCoordinates)
        outState.putSerializable("finishMarkerCoordinates", finishMarkerCoordinates)
        outState.putSerializable("wpMarkerCoordinates", wpMarkerCoordinates)
        outState.putSerializable("cpMarkers", cpMarkers)
        outState.putSerializable("allLocations", allLocations)
    }

    private fun restoreFromSavedInstance(savedInstanceState: Bundle) {
        spinner!!.visibility = View.VISIBLE

        polyline = null
        polylineOptions = PolylineOptions().width(10f).color(Color.MAGENTA)

        Log.d(TAG, "OnRestore, firstboot: $isFirstBoot")
        isTracking = savedInstanceState.getBoolean("isTracking")
        northIsUp = savedInstanceState.getBoolean("northIsUp")
        timerTotal!!.base = savedInstanceState.getLong("totalTimerBase")
        timerCP!!.base = savedInstanceState.getLong("totalTimerCP")
        timerWP!!.base = savedInstanceState.getLong("totalTimerWP")

        findViewById<TextView>(R.id.total_dist_field).text = savedInstanceState.getString("distanceTotal")
        findViewById<TextView>(R.id.total_speed_field).text = savedInstanceState.getString("paceTotal")
        findViewById<TextView>(R.id.wp_distance_field).text = savedInstanceState.getString("distanceWP")
        findViewById<TextView>(R.id.wp_dist_direct_field).text = savedInstanceState.getString("distanceWPDirect")
        findViewById<TextView>(R.id.cp_distance_field).text = savedInstanceState.getString("distanceCP")
        findViewById<TextView>(R.id.cp_dist_direct_field).text = savedInstanceState.getString("distanceCPDirect")

        val startMarkerCoordinatesRestored: ArrayList<Double> = savedInstanceState.getSerializable("startMarkerCoordinates") as ArrayList<Double>

        try {
            markStart(startMarkerCoordinatesRestored[0], startMarkerCoordinatesRestored[1], true)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }

        if (isTracking) {

            timerTotal!!.start()
            timerCP!!.start()
            timerWP!!.start()

            findViewById<TextView>(R.id.startButtonText).text = getString(R.string.stop_button)

        } else {
            val wpMarkerCoordinatesRestored: ArrayList<Double> = savedInstanceState.getSerializable("wpMarkerCoordinates") as ArrayList<Double>
            val cpMarkersRestored: ArrayList<ArrayList<Double>> = savedInstanceState.getSerializable("cpMarkers") as ArrayList<ArrayList<Double>>
            val allLocationsRestored: ArrayList<ArrayList<Double>> = savedInstanceState.getSerializable("allLocations") as ArrayList<ArrayList<Double>>
            val finishMarkerCoordinatesRestored: ArrayList<Double> = savedInstanceState.getSerializable("finishMarkerCoordinates") as ArrayList<Double>

            try {
                markWP(wpMarkerCoordinatesRestored[0], wpMarkerCoordinatesRestored[1], true)
            } catch (e: IndexOutOfBoundsException) {
                e.printStackTrace()
            }
            try {
                cpMarkersRestored.forEach { markCP(it[0], it[1], true) }
            } catch (e: IndexOutOfBoundsException) {
                e.printStackTrace()
            }
            try {
                allLocationsRestored.forEach { extendPolyline(it[0], it[1], true) }
            } catch (e: IndexOutOfBoundsException) {
                e.printStackTrace()
            }
            try {
                markFinish(finishMarkerCoordinatesRestored[0], finishMarkerCoordinatesRestored[1], true)
            } catch (e: IndexOutOfBoundsException) {
                e.printStackTrace()
            }
        }
        spinner!!.visibility = View.GONE
    }


    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                    C.NOTIFICATION_CHANNEL,
                    "Default channel",
                    NotificationManager.IMPORTANCE_LOW
            )

            channel.description = "Default channel"

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }


    private fun resetButtonOnClick() {
        if (isTracking) {
            startButtonOnClick()
        }
        var kIntent = Intent(this, PushGPSPointService::class.java)
        stopService(kIntent)
        var iIntent = Intent(this, DatabaseService::class.java)
        stopService(iIntent)
        var jIntent = Intent(this, GpxService::class.java)
        stopService(jIntent)
        var vIntent = Intent(this, TrackerService::class.java)
        stopService(vIntent)

        if (polyline != null) {
            polyline!!.remove()
        }
        polyline = null
        polylineOptions = PolylineOptions().width(10f).color(Color.MAGENTA)

        mMap.clear()
        northIsUp = true

        findViewById<TextView>(R.id.total_dist_field).text = ""
        findViewById<TextView>(R.id.total_time_field).text = ""
        findViewById<TextView>(R.id.total_speed_field).text = ""

        findViewById<TextView>(R.id.wp_distance_field).text = ""
        findViewById<TextView>(R.id.wp_dist_direct_field).text = ""
        findViewById<TextView>(R.id.wp_time_field).text = ""

        findViewById<TextView>(R.id.cp_distance_field).text = ""
        findViewById<TextView>(R.id.cp_dist_direct_field).text = ""
        findViewById<TextView>(R.id.cp_time_field).text = ""

        wpMarker = null
        startMarkerCoordinates = ArrayList(2)
        finishMarkerCoordinates = ArrayList(2)
        wpMarkerCoordinates = ArrayList(2)
        cpMarkers = ArrayList()
        allLocations = ArrayList()

        savedInstanceState = null
    }

    private fun northUpButtonOnClick() {

        northIsUp = !northIsUp
        mMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(lat, lon)))
        if (!northIsUp) {
            moveCameraAccordingToMovingDirection()
        } else {
            moveCameraAccordingToNorthDirection()
        }
    }

    fun buttonSettingsOnClick(view: View) {
        val wrapper = ContextThemeWrapper(this, R.style.MapMenuStyle)
        val mapMenu = PopupMenu(wrapper, view)
        mapMenu.inflate(R.menu.map_menu)
        mapMenu.setOnMenuItemClickListener(this)

        mapMenu.show()
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.normalMap -> {
                mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                return true
            }
            R.id.satelliteMap -> {
                mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
                return true
            }
            R.id.terrainMap -> {
                mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
                return true
            }

        }
        return false
    }



    private fun startButtonOnClick() {

        if (location == null) {
            val text = "NO GPS SIGNAL DETECTED YET. PLEASE WAIT A FEW SECONDS..."
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(applicationContext, text, duration)
            toast.show()
        } else {
            val mIntent = Intent(this, TrackerService::class.java)

            mIntent.putExtra("lat", location!!.latitude)
            mIntent.putExtra("lng", location!!.longitude)

            if (isTracking) {

                LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
                stopService(mIntent)

                findViewById<TextView>(R.id.startButtonText).text  = getString(R.string.start_button)
                markFinish(0.0, 0.0, false)

                timerTotal!!.stop()
                timerWP!!.stop()
                timerCP!!.stop()

            } else {

                LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, broadcastReceiverIntentFilter)

                resetButtonOnClick()

                mIntent.putExtra(C.FIRST_NAME, firstName)
                mIntent.putExtra(C.LAST_NAME, lastName)
                mIntent.putExtra(C.EMAIL, email)
                mIntent.putExtra(C.TOKEN, token)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(mIntent)
                } else {
                    startService(mIntent)
                }

                findViewById<TextView>(R.id.startButtonText).text = getString(R.string.stop_button)
                markStart(0.0, 0.0, false)

                timerTotal!!.base = SystemClock.elapsedRealtime()
                timerWP!!.base = SystemClock.elapsedRealtime()
                timerCP!!.base = SystemClock.elapsedRealtime()

                timerTotal!!.start()
                timerWP!!.start()
                timerCP!!.start()
            }
            isTracking = !isTracking
        }
    }

    private fun checkpointButtonOnClick() {
        val intent = Intent(C.NOTIFICATION_ACTION_CP)
        sendBroadcast(intent)

        timerCP!!.base = SystemClock.elapsedRealtime()
        timerCP!!.start()
    }

    private fun wayPointButtonOnClick() {
        val intent = Intent(C.NOTIFICATION_ACTION_WP)
        sendBroadcast(intent)

        timerWP!!.base = SystemClock.elapsedRealtime()
        timerWP!!.start()
    }


    private fun markStart(latR: Double, lonR: Double, fromRestore: Boolean) {
        if (!isTracking) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return
            }
            locationManager.requestLocationUpdates(provider!!, 500, 1f, this)
            var cpLatLng = LatLng(location!!.latitude, location!!.longitude)
            if (fromRestore) {
                cpLatLng = LatLng(latR, lonR)
            }
            mMap.addMarker(MarkerOptions().position(cpLatLng).title("START").icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpointstart)))
            mMap.moveCamera(CameraUpdateFactory.zoomTo(15f))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(cpLatLng))

            if (fromRestore) {
                startMarkerCoordinates.add(latR)
                startMarkerCoordinates.add(lonR)
            } else {
                startMarkerCoordinates.add(location!!.latitude)
                startMarkerCoordinates.add(location!!.longitude)
            }
        } else if (isTracking && fromRestore) {
            val cpLatLng = LatLng(latR, lonR)
            mMap.addMarker(MarkerOptions().position(cpLatLng).title("START").icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpointstart)))
            startMarkerCoordinates.add(latR)
            startMarkerCoordinates.add(lonR)
        }
    }

    private fun markFinish(latR: Double, lonR: Double, fromRestore: Boolean) {
        if (isTracking) {
            var cpLatLng = LatLng(lat, lon)
            if (fromRestore) {
                cpLatLng = LatLng(latR, lonR)
            }
            mMap.addMarker(MarkerOptions().position(cpLatLng).title("FINISH").icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpointfinish)))
            if (fromRestore) {
                finishMarkerCoordinates.add(latR)
                finishMarkerCoordinates.add(lonR)
            } else {
                finishMarkerCoordinates.add(lat)
                finishMarkerCoordinates.add(lon)
            }
        } else if (!isTracking && fromRestore) {
            Log.d(TAG, "recreating finish Marker $latR , $lonR")
            val finLatLng = LatLng(latR, lonR)
            mMap.addMarker(MarkerOptions().position(finLatLng).title("FINISH").icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpointfinish)))
            finishMarkerCoordinates.add(latR)
            finishMarkerCoordinates.add(lonR)
        }
    }

    fun markCP(latR: Double, lonR: Double, fromRestore: Boolean) {
        if (isTracking) {

            var cpLatLng = LatLng(lat, lon)
            if (fromRestore) {
                cpLatLng = LatLng(latR, lonR)
            }
            mMap.addMarker(MarkerOptions().position(cpLatLng).title("CP").icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpointcp)))

            if (fromRestore) {
                cpMarkers.add(arrayListOf(latR, lonR))
            } else {
                cpMarkers.add(arrayListOf(lat, lon))
            }

            if (!fromRestore) {
                timerCP!!.base = SystemClock.elapsedRealtime()
                timerCP!!.start()
            }
        } else if (!isTracking && fromRestore) {
            val cpLatLng = LatLng(latR, lonR)
            mMap.addMarker(MarkerOptions().position(cpLatLng).title("CP").icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpointcp)))
            cpMarkers.add(arrayListOf(latR, lonR))
        }
    }

    fun markWP(latR: Double, lonR: Double, fromRestore: Boolean) {
        if (isTracking) {
            if (wpMarker != null) {
                wpMarker!!.remove()
            }
            var wpLatLng = LatLng(lat, lon)
            if (fromRestore) {
                wpLatLng = LatLng(latR, lonR)
            }
            wpMarker = mMap.addMarker(MarkerOptions().position(wpLatLng).title("WP").icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpointwp)))

            if (fromRestore) {
                wpMarkerCoordinates = ArrayList(2)
                wpMarkerCoordinates.add(latR)
                wpMarkerCoordinates.add(lonR)
            } else {
                wpMarkerCoordinates = ArrayList(2)
                wpMarkerCoordinates.add(lat)
                wpMarkerCoordinates.add(lon)
            }

            if (!fromRestore) {
                timerWP!!.base = SystemClock.elapsedRealtime()
                timerWP!!.start()
            }
        } else if (!isTracking && fromRestore) {
            val wpLatLng = LatLng(latR, lonR)
            wpMarker = mMap.addMarker(MarkerOptions().position(wpLatLng).title("WP").icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpointwp)))
            wpMarkerCoordinates = ArrayList(2)
            wpMarkerCoordinates.add(latR)
            wpMarkerCoordinates.add(lonR)
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        mMap.isMyLocationEnabled = true

        var markerLatLng = LatLng(59.4148365, 24.6491961)
        if (location != null) {
            markerLatLng = LatLng(location!!.latitude, location!!.longitude)
        }
        mMap.moveCamera(CameraUpdateFactory.zoomTo(15f))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(markerLatLng))
        if (!northIsUp) {
            moveCameraAccordingToMovingDirection()
        } else {
            moveCameraAccordingToNorthDirection()
        }

        if (savedInstanceState != null) {
            restoreFromSavedInstance(savedInstanceState!!)
        }
    }

    override fun onLocationChanged(location: Location) {
        this.location = location
    }

    private fun extendPolyline(latR: Double, lonR: Double, fromRestore: Boolean) {
        var position = LatLng(lat, lon)
        if (fromRestore) {
            position = LatLng(latR, lonR)
        }
        if (fromRestore) {
            allLocations.add(arrayListOf(latR, lonR))
        } else {
            allLocations.add(arrayListOf(lat, lon))
        }


        if (isTracking || fromRestore) {
            if (polyline != null) {
                polyline!!.remove()
            }
            polylineOptions.add(position)
            polyline = mMap.addPolyline(polylineOptions)
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLng(position))
        if (!northIsUp) {
            moveCameraAccordingToMovingDirection()
        } else {
            moveCameraAccordingToNorthDirection()
        }
    }

    private fun moveCameraAccordingToMovingDirection() {
        val cameraPos = CameraPosition
                .builder(mMap.cameraPosition)
                .bearing(location!!.bearing)
                .build()
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPos))
    }

    private fun moveCameraAccordingToNorthDirection() {
        val cameraPos = CameraPosition
                .builder(mMap.cameraPosition)
                .bearing(0.0F)
                .build()
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPos))
    }



    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor === accelerometer) {
            lowPass(event.values, lastAccelerometer)
            lastAccelerometerSet = true
        } else if (event.sensor === magnetometer) {
            lowPass(event.values, lastMagnetometer)
            lastMagnetometerSet = true
        }

        if (lastAccelerometerSet && lastMagnetometerSet) {
            val r = FloatArray(9)
            if (SensorManager.getRotationMatrix(r, null, lastAccelerometer, lastMagnetometer)) {
                val orientation = FloatArray(3)
                SensorManager.getOrientation(r, orientation)
                val degree = (Math.toDegrees(orientation[0].toDouble()) + 360).toFloat() % 360

                val rotateAnimation = RotateAnimation(
                        currentDegree,
                        -degree,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f)
                rotateAnimation.duration = 1000
                rotateAnimation.fillAfter = true

                compassImage.startAnimation(rotateAnimation)
                currentDegree = -degree
            }
        }
    }

    private fun lowPass(input: FloatArray, output: FloatArray) {
        val alpha = 0.05f

        for (i in input.indices) {
            output[i] = output[i] + alpha * (input[i] - output[i])
        }
    }

    private fun onCompassBtnOnClick() {
        if (compassImage.alpha > 0.9F) {
            compassImage.alpha = 0.0F
        } else {
            compassImage.alpha = 1.0F
        }
    }

    fun updateGpsFields(intent: Intent) {
//        Log.d(TAG, "updateGpsFields")

        if (!fromResume) {
            lat = intent.getDoubleExtra(C.LOCATION_UPDATE_LATITUDE, 0.0)
            lon = intent.getDoubleExtra(C.LOCATION_UPDATE_LONGITUDE, 0.0)

            extendPolyline(0.0, 0.0, false)

        } else {
            restoreMap(intent)
            fromResume = false
        }

    }

    private fun restoreMap(intent: Intent) {
        Log.d(TAG, "restoreMap")
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        spinner!!.visibility = View.VISIBLE

        polyline = null
        polylineOptions = PolylineOptions().width(10f).color(Color.MAGENTA)

        wpMarkerCoordinates = ArrayList(2)
        cpMarkers = ArrayList()
        allLocations = ArrayList()

        val wpMarkerCoordinatesRestored: ArrayList<Double> = intent.getSerializableExtra(C.ONRESUME_WP_MARKER) as ArrayList<Double>
        val cpMarkersRestored: ArrayList<ArrayList<Double>> = intent.getSerializableExtra(C.ONRESUME_CP_MARKERS) as ArrayList<ArrayList<Double>>
        val allLocationsRestored: ArrayList<ArrayList<Double>> = intent.getSerializableExtra(C.ONRESUME_GENERAL_MARKERS) as ArrayList<ArrayList<Double>>

        try {
            markWP(wpMarkerCoordinatesRestored[0], wpMarkerCoordinatesRestored[1], true)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }

        try {
            cpMarkersRestored.forEach { markCP(it[0], it[1], true) }
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }

        try {
            allLocationsRestored.forEach { extendPolyline(it[0], it[1], true) }
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, broadcastReceiverIntentFilter)
        spinner!!.visibility = View.GONE
    }

    fun updateBottomUIFields(intent: Intent) {
        findViewById<TextView>(R.id.total_dist_field).text = intent.getStringExtra(C.LOCATION_UPDATE_TOTAL_DISTANCE)
        findViewById<TextView>(R.id.total_speed_field).text = intent.getStringExtra(C.LOCATION_UPDATE_TOTAL_SPEED)

        findViewById<TextView>(R.id.wp_distance_field).text = intent.getStringExtra(C.LOCATION_UPDATE_WP_DISTANCE_TOTAL)
        findViewById<TextView>(R.id.wp_dist_direct_field).text = intent.getStringExtra(C.LOCATION_UPDATE_WP_DISTANCE_DIRECT)

        findViewById<TextView>(R.id.cp_distance_field).text = intent.getStringExtra(C.LOCATION_UPDATE_CP_DISTANCE_TOTAL)
        findViewById<TextView>(R.id.cp_dist_direct_field).text = intent.getStringExtra(C.LOCATION_UPDATE_CP_DISTANCE_DIRECT)

        if (fromResume) {
            timerWP!!.base = intent.getLongExtra(C.TIMER_WP_BASE, System.currentTimeMillis())
            timerWP!!.start()

            timerCP!!.base = intent.getLongExtra(C.TIMER_CP_BASE, System.currentTimeMillis())
            timerCP!!.start()
        }
    }


    private inner class InnerBroadcastReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
//            Log.d(TAG, intent!!.action.toString())
            when (intent!!.action){
                C.LOCATION_UPDATE_ACTION -> {
                    updateBottomUIFields(intent)
                    updateGpsFields(intent)
                }
                C.LOCATION_MARK_CP_ACTION -> {
                    markCP(0.0, 0.0, false)
                }
                C.LOCATION_MARK_WP_ACTION -> {
                    markWP(0.0, 0.0, false)
                }
            }
        }

    }


}
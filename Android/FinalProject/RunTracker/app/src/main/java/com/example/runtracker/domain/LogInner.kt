package com.example.runtracker.domain

import kotlinx.serialization.Serializable

@Serializable
class LogInner {
    var email: String = ""
    var password: String = ""

    constructor(email: String, password: String) {
        this.email = email
        this.password = password
    }
}
package com.example.runtracker.domain

import androidx.room.*


@Dao
interface PersonDao {

    @Transaction
    @Insert
    fun insertPerson(person: Person): Long

    @Insert
    fun insertActivities(activities: List<Activity>)

    @Insert
    fun insertBackEndSyncs(backEndSyncs: List<BackEndSync>)

    @Insert
    fun insertBackEndSync(backEndSync: BackEndSync)

    @Insert
    fun insertActivity(activity: Activity)

    @Transaction
    @Query("SELECT * FROM person")
    fun getAll(): List<Person>

    @Transaction
    @Query("SELECT * FROM activity WHERE id_fkPerson IN (:personID)")
    fun findActivitiesByPersonId(personID: Int): List<Activity>


    @Query("SELECT * FROM person WHERE email IN (:email) AND firstname IN (:firstname)")
    fun getPersonInFocus(email: String, firstname: String): List<Person>

    @Query("SELECT * FROM activity WHERE id_activity IN (:id)")
    fun getActivityById(id: Int): List<Activity>

    @Query("SELECT * FROM backendsync WHERE sessionId IN (:id) AND NOT isSynced LIMIT 10 ")
    fun getBackEndSyncBySessionId(id: String): List<BackEndSync>

    @Query("SELECT * FROM backendsync WHERE sessionId IN (:id) AND NOT isSynced")
    fun getBackEndSyncBySessionIdAll(id: String): List<BackEndSync>

    @Query("SELECT COUNT(*) FROM backendsync WHERE sessionId IN (:id) AND NOT isSynced")
    fun getBackEndPointsTableSize(id: String): Int


    @Query("SELECT * FROM backendsync WHERE id_backEndSync == :id")
    fun getBackEndTableLine(id: Int): BackEndSync

    @Update
    fun updateBackEndTableLine(line: BackEndSync?): Int

    fun updateBackEndSync(id: Int, newStatus: Boolean) {
        val line: BackEndSync = getBackEndTableLine(id)
        line.isSynced = newStatus
        updateBackEndTableLine(line)
    }

    @Query("SELECT * FROM activity WHERE sessionId == :id")
    fun getBackActivity(id: String): Activity

    @Update
    fun updateActivitySessionId(line: Activity?): Int

    fun updateActivityNewSessionId(id: String, newSessId: String) {
        val line: Activity = getBackActivity(id)
        line.setSessionId(newSessId)
        updateActivitySessionId(line)
    }

    @Query("SELECT * FROM backendsync WHERE sessionId == :id")
    fun getBackEndSyncsBySessionId(id: String): List<BackEndSync>

    @Update
    fun updateAllBackEndSyncsBySessionId(lines: List<BackEndSync>?): Int

    fun updateAllBackEndSyncSessionIdBySessionId(sessionId: String, newSessionId: String) {
        val lines: List<BackEndSync> = getBackEndSyncsBySessionId(sessionId)
        lines.forEach {
            it.sessionId = newSessionId
        }
        updateAllBackEndSyncsBySessionId(lines)
    }

    @Transaction
    @Delete
    fun deleteActivity(activity: Activity)

}
//
//  GradientView.swift
//  Connect4
//
//  Created by Mihkel Heinmaa on 03.11.2020.
//

import UIKit

@IBDesignable
class GradientView: UIView {

        @IBInspectable var startColor: UIColor = UIColor.clear
        @IBInspectable var endColor: UIColor = UIColor.clear

        override func draw(_ rect: CGRect) {
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.frame = CGRect(x: CGFloat(0),
                                    y: CGFloat(0),
                                    width: superview!.frame.size.width,
                                    height: superview!.frame.size.height)
            gradient.colors = [startColor.cgColor, endColor.cgColor]
            gradient.zPosition = -1
            layer.addSublayer(gradient)
        }


}

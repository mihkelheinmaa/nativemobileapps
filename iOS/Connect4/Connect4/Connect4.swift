//
//  Connect4.swift
//  Connect4
//
//  Created by Mihkel Heinmaa on 03.11.2020.
//

import Foundation

class Connect4 {

    var won = false
    
    var gameBoard: [[Tile?]] = Array(repeating: Array(repeating: Tile(color: "none"), count: 6), count: 7)

    var nextMoveByRed = true

    var slotsAvailable = 42
    var turnsUsed = 0


    func getFirstUnusedTile(colNo col:Int, rowNo row:Int) -> (Int) {
        for i in (0...5).reversed() {
            if gameBoard[col][i]?.color == "none" {
                return i
            }
        }
        return -1
    }
    
    func getTile (colNo col:Int, rowNo row:Int) -> Tile? {
        return gameBoard[col][row]
    }

    func move(colNo col:Int, rowNo row:Int) -> String {
        let rowNr = getFirstUnusedTile(colNo: col, rowNo: row)
        if  rowNr != -1 {
            if nextMoveByRed {
                gameBoard[col][rowNr]?.color = "red"
            } else {
                gameBoard[col][rowNr]?.color = "yellow"
            }
            if !nextMoveByRed {
                turnsUsed += 1
            }
            slotsAvailable -= 1
            
            if checkForWin(colNo: col, rowNo: rowNr) {
                return "win"
            }
            nextMoveByRed = !nextMoveByRed
            return "next"
        }
        return "columnFull"
    }

    func checkForWin(colNo col:Int, rowNo row:Int) -> Bool {
        var color = "yellow"
        if nextMoveByRed {
            color = "red"
        }

        if checkForWinHorizontally(colNo: col, rowNo: row, slotColor: color) {
            return true
        }
        if checkForWinVertically(colNo: col, rowNo: row, slotColor: color){
            return true
        }
        if checkForWinDiagonally(colNo: col, rowNo: row, slotColor: color) {
            return true
        }
        
        return false
    }

    func checkForWinHorizontally(colNo col:Int, rowNo row:Int, slotColor color:String) -> Bool {
        var count = 0
        var best = 0
        var winningTilesTemp = [[Int]]()
        var winningTiles = [[Int]]()
        
        for i in 0...6 {
            if gameBoard[i][row]?.color != color {
                if count > best {
                    best = count
                    winningTiles = winningTilesTemp
                }
                count = 0
                winningTilesTemp.removeAll()
            } else {
                count += 1
                winningTilesTemp.append([i, row])
            }
        }
        if best >= 4 || count >= 4 {
            if winningTiles.count < winningTilesTemp.count {
                winningTiles = winningTilesTemp
            }
            getWinningTiles(winningTiles: winningTiles, direction: "horizontal")
            return true
        }
        return false
    }

    func checkForWinVertically(colNo col:Int, rowNo row:Int, slotColor color:String) -> Bool {
        var count = 0
        var best = 0
        var winningTilesTemp = [[Int]]()
        var winningTiles = [[Int]]()
        
        for i in 0...5 {
            if gameBoard[col][i]?.color != color {
                if count > best {
                    best = count
                    winningTiles = winningTilesTemp
                }
                count = 0
                winningTilesTemp.removeAll()
            } else {
                count += 1
                winningTilesTemp.append([col, i])
            }
        }
        if best >= 4 || count >= 4 {
            if winningTiles.count < winningTilesTemp.count {
                winningTiles = winningTilesTemp
            }
            getWinningTiles(winningTiles: winningTiles, direction: "vertical")
            return true
        }
        return false
    }

    func checkForWinDiagonally(colNo col:Int, rowNo row:Int, slotColor color:String) -> Bool {
        var matchingTilesNW = [[Int]]()
        var matchingTilesNE = [[Int]]()
        var matchingTilesSE = [[Int]]()
        var matchingTilesSW = [[Int]]()
                
        var column = col
        var rowIn = row
        repeat {
            column += 1
            if column == 7 {
                break
            }
            rowIn += 1
            if rowIn == 6 {
                break
            }

            if gameBoard[column][rowIn]?.color == color {
                matchingTilesSE.append([column, rowIn])
            } else {
                break
            }
        } while gameBoard[column][rowIn]?.color == color

        column = col
        rowIn = row
        repeat {
            column += 1
            if column == 7 {
                break
            }
            rowIn -= 1
            if rowIn == -1 {
                break
            }

            if gameBoard[column][rowIn]?.color == color {
                matchingTilesNE.append([column, rowIn])
            } else {
                break
            }
        } while gameBoard[column][rowIn]?.color == color

        column = col
        rowIn = row
        repeat {
            column -= 1
            if column == -1 {
                break
            }
            rowIn -= 1
            if rowIn == -1 {
                break
            }

            if gameBoard[column][rowIn]?.color == color {
                matchingTilesNW.append([column, rowIn])
            } else {
                break
            }
        } while gameBoard[column][rowIn]?.color == color

        column = col
        rowIn = row
        repeat {
            column -= 1
            if column == -1 {
                break
            }
            rowIn += 1
            if rowIn == 6 {
                break
            }

            if gameBoard[column][rowIn]?.color == color {
                matchingTilesSW.append([column, rowIn])
            } else {
                break
            }
        } while gameBoard[column][rowIn]?.color == color
        
        if matchingTilesNE.count + matchingTilesSW.count >= 3 {
            getWinningTiles(winningTiles: matchingTilesNE + matchingTilesSW + [[col, row]], direction: "")
            return true
        }
        if matchingTilesNW.count + matchingTilesSE.count >= 3 {
            getWinningTiles(winningTiles: matchingTilesNW + matchingTilesSE + [[col, row]], direction: "")
            return true
        }
        return false
    }
    
    
    
    func getWinningTiles(winningTiles tiles:[[Int]], direction dir:String) {
        for tile in tiles {
            gameBoard[tile[0]][tile[1]]?.winningTile = true
        }
    }


    func reset() {
        gameBoard = Array(repeating: Array(repeating: Tile(color: "none"), count: 6), count: 7)
        nextMoveByRed = true
        slotsAvailable = 42
        turnsUsed = 0
        won = false
    }
}

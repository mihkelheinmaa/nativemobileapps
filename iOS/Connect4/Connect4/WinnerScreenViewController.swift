//
//  WinnerScreenViewController.swift
//  Connect4
//
//  Created by Mihkel Heinmaa on 21.11.2020.
//

import UIKit

class WinnerScreenViewController: UIViewController {

    @IBOutlet var background: GradientView!
    @IBOutlet weak var winnerLabel: UILabel!
    var winnerIsRed = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !winnerIsRed {
            winnerLabel.text = "🟡 WON!"
            
            background.endColor = #colorLiteral(red: 1, green: 0.8323456645, blue: 0.4732058644, alpha: 1)
            background.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        
        

        // Do any additional setup after loading the view.
    }
    
    func setWinner(winner: String) {
        if winner == "y" {
            winnerIsRed = false
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

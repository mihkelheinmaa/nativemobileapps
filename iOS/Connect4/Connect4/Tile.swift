//
//  Tile.swift
//  Connect4
//
//  Created by Mihkel Heinmaa on 03.11.2020.
//

import Foundation

struct Tile {
    var color: String
    var winningTile: Bool = false
    
    init(color: String) {
        self.color = color
    }

}

//
//  ViewController.swift
//  Connect4
//
//  Created by Mihkel Heinmaa on 03.11.2020.
//

import UIKit

class ViewController: UIViewController {
    
    var game: Connect4 = Connect4()
    var timer = Timer()
    var timerDisplayed = 0
    var timerIsRunning = false
    
    @IBOutlet var MainScreenBackground: GradientView!
    
    @IBOutlet var BoardSlots: [UIButton]!
    
    @IBOutlet weak var NextMoveByIconLabel: UILabel!
    
    @IBOutlet weak var TurnsUsedLabel: UILabel!
    
    @IBOutlet weak var EmptySlotsLabel: UILabel!
    
    @IBOutlet weak var InfoLabel: UILabel!
    
    @IBOutlet weak var TimerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        for slot in BoardSlots {
            slot.setBackgroundImage(UIImage(named: "EmptyC") as UIImage?, for: UIControl.State.normal)
        }
    }
    
    
    
    @IBAction func RestartActionOnClick(_ sender: UIButton) {
        game.reset()
        timerIsRunning = false
        timer.invalidate()
        timerDisplayed = 0
        TimerLabel.text = "00:00"
        
        for slot in BoardSlots {
            slot.setBackgroundImage(UIImage(named: "EmptyC") as UIImage?, for: UIControl.State.normal)
            slot.alpha = 1
        }
        NextMoveByIconLabel.numberOfLines = 1
        NextMoveByIconLabel.adjustsFontSizeToFitWidth = true
        NextMoveByIconLabel.minimumScaleFactor = 0.1
        NextMoveByIconLabel.text = "🔴"
        TurnsUsedLabel.numberOfLines = 1
        TurnsUsedLabel.adjustsFontSizeToFitWidth = true
        TurnsUsedLabel.minimumScaleFactor = 0.1
        TurnsUsedLabel.text = String(game.turnsUsed)
        EmptySlotsLabel.numberOfLines = 1
        EmptySlotsLabel.adjustsFontSizeToFitWidth = true
        EmptySlotsLabel.minimumScaleFactor = 0.1
        EmptySlotsLabel.text = String(game.slotsAvailable)
        InfoLabel.text = ""
        MainScreenBackground.startColor = #colorLiteral(red: 0.8321695924, green: 0.985483706, blue: 0.4733308554, alpha: 1)
        MainScreenBackground.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        
    }
    
    
    @IBAction func GameSlotClicked(_ sender: UIButton) {
        if game.won == false {
            if !timerIsRunning {
                timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TimerAction), userInfo: nil, repeats: true)
                timerIsRunning = true
            }
            
            
            let (col, row) = getRowCol(tileNo: sender.tag)
            let moveResult = game.move(colNo: col, rowNo: row)
            InfoLabel.text = ""
            switch moveResult {
            case "columnFull":
                InfoLabel.numberOfLines = 1
                InfoLabel.adjustsFontSizeToFitWidth = true
                InfoLabel.minimumScaleFactor = 0.1
                InfoLabel.text = "This column is already full! Try again!"
                updateGameState(colNo: col)
            case "win":
                timerIsRunning = false
                timer.invalidate()
                InfoLabel.text = game.nextMoveByRed ? "🔴 WON!" : "🟡 WON!"
                MainScreenBackground.startColor = game.nextMoveByRed ? UIColor.red : #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                MainScreenBackground.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                updateGameState(colNo: col)
                reDrawWinnerBoard()
                game.won = true
                loadWinnerNav(winner: game.nextMoveByRed ? "r" : "y")
                return
            default:
                updateGameState(colNo: col)
            }
            if game.slotsAvailable == 0 {
                InfoLabel.numberOfLines = 1
                InfoLabel.adjustsFontSizeToFitWidth = true
                InfoLabel.minimumScaleFactor = 0.1
                InfoLabel.text = "No empty slots left. Game over."
            }
        }
        
    }
    
    func loadWinnerNav(winner: String) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "winView") as! WinnerScreenViewController
        nextViewController.setWinner(winner: winner)
        self.present(nextViewController, animated: true)
    }
    
    func reDrawWinnerBoard() {
        for slot in BoardSlots {
            let (col, row) = getRowCol(tileNo: slot.tag)
            let tile = game.getTile(colNo: col, rowNo: row)
            if tile?.winningTile != true {
                if tile?.color != nil {
                    slot.alpha = 0.2
                }
            }
        }
    }
    
    
    func updateGameState(colNo: Int) {
        for slot in BoardSlots {
            let (col, row) = getRowCol(tileNo: slot.tag)
            if col == colNo {
                let tile = game.getTile(colNo: col, rowNo: row)
                if tile?.color == "red" {
                    slot.setBackgroundImage(UIImage(named: "redC") as UIImage?, for: UIControl.State.normal)
                } else if tile?.color == "yellow" {
                    slot.setBackgroundImage(UIImage(named: "yellowC") as UIImage?, for: UIControl.State.normal)
                }
            }
        }
        NextMoveByIconLabel.text = game.nextMoveByRed ? "🔴" : "🟡"
        TurnsUsedLabel.text = String(game.turnsUsed)
        EmptySlotsLabel.text = String(game.slotsAvailable)
    }
    
    
    func getRowCol(tileNo: Int) -> (col: Int, row: Int) {
        let rowNo = tileNo / 7
        let colNo = tileNo - rowNo * 7
        
        return (colNo, rowNo)
    }
    
    @objc func TimerAction() {
        timerDisplayed += 1
        
        let minutes = Int(timerDisplayed/60)
        var minuteString = "\(minutes)"
        if minutes < 10  {
            minuteString = "0\(minutes)"
        }
        
        let seconds = Int(timerDisplayed % 60)
        var secondString = "\(seconds)"
        if seconds < 10  {
            secondString = "0\(seconds)"
        }
        
        TimerLabel.text = "\(minuteString):\(secondString)"
    }

}

